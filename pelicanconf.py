#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Viviane Pons'
SITENAME = u'Viviane Pons'
SITEURL = ''
SITESUBTITLE = u'Maîtresse de conférences, Université Paris-Saclay, Orsay'
SITESEO = u'Viviane Pons - LISN Université Paris-Saclay'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

THEME = 'theme'
BOOTSTRAP = 'bootstrap-3.3.7-dist'
FONTAWESOME = 'font-awesome-4.6.3'

PLUGIN_PATHS = ['plugins/']
PLUGINS = [
    'drop-down-menu',
    'i18n_subsites',
    'pelican-page-order',
    'render_math',
    'pelican-bibtex',
    'articles-on-page',
    'custom-date-fields'

]

INDEX_SAVE_AS = 'blog_index.html'

I18N_SUBSITES = {
    'en': {
        'SITENAME': u'Viviane Pons',
        'SITESEO': u'Viviane Pons - LISN Paris-Saclay University, Orsay',
        'SITESUBTITLE' : u'Associate Professor, Paris-Saclay University, Orsay'
        }
    }

I18N_UNTRANSLATED_ARTICLES = 'keep'
I18N_UNTRANSLATED_PAGES = 'keep'

STATIC_PATHS = ["images","bib","docs","ens","static"]
ARTICLE_EXCLUDES = STATIC_PATHS

DISPLAY_CATEGORIES_ON_MENU = False


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


DEFAULT_PAGINATION = 10
ARTICLES_ON_PAGE = 5

PUBLICATIONS_SRC = "content/bib/"

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

CUSTOM_DATE_FIELDS = ["conference_date_start", "conference_date_end"]


PUBLIKIND = {
    'fr': {
        "article": "Articles journaux",
        "inproceedings": "Articles conférences",
        "incollection": "Chapitres de livre",
        "techreport": "Rapport",
        "vulg": "Vulgarisation",
        "misc": "Preprints",
        "book": "Livres",
        "phdthesis": "Thèse",
        "proceedings": "Edited proceedings"
    },
    'en': {
        "article": "Journal Papers",
        "inproceedings": "Conference Papers",
        "incollection": "Book Sections",
        "techreport": "Reports",
        "vulg": "Vulgarization",
        "misc": "Preprints",
        "book": "Books",
        "phdthesis": "Thesis",
        "proceedings": "Edited proceedings"
    },

}

CATEGORY_NAMES = {
    "fr": {
        "talks-posters": "Catégorie : exposés et posters",
        "conferences": "Catégorie : conférences",
        "teaching": "Catégorie : enseignement"
    },
    "en": {
        "talks-posters":"Category: talks and posters",
        "conferences": "Category: confences",
        "teaching": "Catégory: teaching"
    }
}

## hack for broken date_locale

DATE_HACK_FORMATS = {
    "fr": {
        "full":'%a %d %B %Y',
        "short":'%d %B %Y',
        "yearless":'%d %B'
    },
    "en": {
        "full": '%m/%d/%Y',
        "short": '%m/%d/%Y',
        "yearless": '%m/%d'
    }
}


