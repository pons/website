Title: Permutrees
date: 2017-08-31 00:10
slug: 2017-08-31-permutrees-talk
lang: en
location: Vienna
conference_name: EuroComb
conference_date_start: 2017-08-28
conference_date_end: 2017-09-01
conference_url: http://www.dmg.tuwien.ac.at/eurocomb2017/
slides: 2017_Eurocomb_permutrees.pdf
tags: Eurocomb, Permutrees


The permutrees are a family of combinatorial objects which includes permutations, 
binary trees, binary sequences and a range of objects "in between". In this talk,
we will mostly discuss their combinatorial properties, especially the lattice structure.
We will also show the polytopes that can be constructed from this combinatorics 
which include the permutaheadron and associahedron. 
