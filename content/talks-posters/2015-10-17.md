Title: Enseigner avec SageMathCloud : retour sur expérience
date: 2015-10-17 00:00
slug: 2015-10-17-talk
lang: fr
location: Pau
conference_name: PyConFr
conference_date_start: 2015-10-17
conference_date_end: 2015-10-20
conference_url: http://www.pycon.fr/2015/schedule/presentation/16/
tags: PyCon, Sage, SageMathCloud
slides: presentation_pyconfr2015.pdf


La plate-forme SageMathCloud offre des possibilités très intéressantes de partage de code pour Sage et python. En particulier, elle peut être utilisée dans le cadre d'un cour interactif. Nous expliquons ici quel a été l'impact de l'outil pour une option Math-Informatique en première année de licence.
