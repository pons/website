Title: Multivariate Polynomials in Sage
date: 2011-03-07 00:00
slug: 2011-03-07-talk
lang: en
location: Ellwangen
conference_name: Séminaire Lotharingien de combinatoire 66
conference_date_start: 2011-03-06
conference_date_end: 2011-03-09
conference_url: http://www.emis.de/journals/SLC/wpapers/s66preface.html
tags: SLC, Sage, Multivariate polynomials
slides: presentation_SLC.pdf
demo_int_link: Demo_multivariate_polynomials.sws




