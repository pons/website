Title: Short Schubert polynomials demo
date: 2013-02-12 00:10
slug: 2013-02-12-talk-2
lang: en
location: ICERM, Providence
conference_name: Sage Days 45
conference_date_start: 2013-02-11
conference_date_end: 2013-02-15
conference_url: http://icerm.brown.edu/sp-s13-w1
tags: Sage, Multivariate polynomials
demo_int_link: Short_Schubert_polynomials_presentation.sws




