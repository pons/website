Title: The Rise-Contact involution on Tamari intervals
date: 2018-07-24 00:10
slug: 2018-07-24-rise-contact-talk
lang: en
location: Providence
conference_name: SageDays@ICERM
conference_url: https://icerm.brown.edu/topical_workshops/tw18-1-sage/
demo_ext_link: https://github.com/VivianePons/public-notebooks/tree/master/TamariIntervalPosets
tags: Interval-posets, Rise-contact involution, Tamari, Sage, ICERM, Video
video: https://icerm.brown.edu/video_archive/?play=1646

We present an involution on intervals of the Tamari lattice which exchanges the "rise" and "contact" statistic. We will use Tamari interval-posets which are implemented in Sage. The talk will be illustrated by Sage examples and computations.
