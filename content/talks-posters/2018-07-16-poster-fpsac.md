Title: Algebraic structures on integer posets
date: 2018-07-16 00:00
slug: 2015-07-16-poster
lang: en
location: Hanover New Hampshire
conference_name: FPSAC
conference_date_start: 2018-07-16
conference_date_end: 2018-07-20
conference_url: https://sites.google.com/view/fpsac2018/home?authuser=0
tags: Posets Lattice, Hopf Algebra, FPSAC
poster: poster_FPSAC2018.pdf




