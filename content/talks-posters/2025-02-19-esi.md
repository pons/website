Title: A Conjecture on descents, inversions, and the weak order
date: 2025-02-19 00:10
slug: 2022-02-19-esi
lang: en
location: ESI, Vienna
conference_name: Recent Perspectives on Non-crossing Partitions through Algebra, Combinatorics, and Probability
conference_url: https://www.esi.ac.at/events/e548/
video: https://youtu.be/c8VntF97x-I
slides: 2025_ESI.pdf
tags: weak order, Coxeter groups, bipartitions, Video

We discuss the notion of partitions of elements in an arbitrary Coxeter system: a partition of an element w is the set of elements such that the left descents of w are the disjoint union of the left descents of the elements in the partition. This is related to the weak order of a Coxeter system and leads to an interesting conjecture that the number of right descents in w is the sum of the numbers of descents in the partition.


