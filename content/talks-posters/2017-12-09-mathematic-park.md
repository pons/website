Title: Les nombres de Catalan
date: 2017-12-09 00:00
slug: 2017-12-09-talk
lang: fr
location: IHP
conference_name: Mathematic Park
conference_url: http://www.ihp.fr/fr/seminaire/mathpark-programme
video: https://www.youtube.com/watch?v=aqQjrHeHXP4
tags: Catalan numbers,  Science popularization, Video


Comptons le nombre de chemins au dessus d'une ligne. Comptons le nombre d'arbres binaires. Comptons le nombre de triangulations d'un polygone régulier. Les mêmes nombres apparaissent : 1, 2, 5, 42... Comment en déterminer la formule ? Lors de cet exposé, nous explorerons cette curiosité des mathématiques et, en détaillant les calculs et les preuves, aborderons les notions de base de la combinatoire énumérative et bijective.


<div style="max-width: 560px; max-height:315px;margin:auto;">
<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/aqQjrHeHXP4" allowfullscreen></iframe>
</div>
</div>

