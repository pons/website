Title: Rise-Contact involution on Tamari intervals
date: 2018-09-28 00:10
slug: 2018-09-28-rise-contact-talk
lang: en
location: Montréal
conference_name: Algèbre et combinatoire au LaCIM
conference_url: http://www.crm.umontreal.ca/2018/Algebre18/
demo_ext_link: https://github.com/VivianePons/public-notebooks/
tags: Interval-posets, Rise-contact involution, Tamari


We present an involution on intervals of the Tamari lattice which exchanges the “rise”and “contact” statistic. The talk will be illustrated by computations and examples in thesoftware SageMath.
