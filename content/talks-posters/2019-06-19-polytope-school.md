Title: The s-Weak order and and s-Permutohedron 
date: 2019-06-19
slug: 2019-06-19-s-weak-order
lang: en
location: Jussieu
conference_name: Summer School on Geometric and Algebraic Combinatorics
conference_url: http://gac-school.imj-prg.fr/
slides: 2019_fpsac.pdf
tags: S-weak-order, s-permutohedra, Tamari, Permutohedron, Associahedron


We present a partial order on some decreasing trees which generalizes the weak order on permutations. This is motivated by similar generalizations on the Tamari lattice. We prove that this new order is always a lattice and contains the nu-Tamari lattice as a sublattice. Besides, it has very interesting geometrical properties which leads to beautiful constructions and conjectures. 
