Title: Presentation of OpenDreamKit
date: 2017-10-19 01:00
slug: 2017-10-19-talk
lang: en
location: Kaiserslautern
conference_name: NetMath workshop
conference_website: https://www.netmath.de
tags: Sage, Jupyter, OpenDreamKit
slides: 2017_Kaiserslautern.pdf


Presenting [OpenDreamKit](https://www.lri.fr/~pons/pages/opendreamkit.html) to the [Netmath community](https://www.netmath.de/).
