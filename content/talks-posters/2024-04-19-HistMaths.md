Title: Expérimentation manuelle et par ordinateur dans la recherche en combinatoire
date: 2024-04-19 08:00
slug: 2024-04-19-talk
lang: fr
location: ITEM
conference_name: Journée d’étude « Représentations exploratoires en mathématiques »
slides: 2024_HistMath.pdf
conference_url: http://www.item.ens.fr/journee-detude-representations-exploratoires-en-mathematiques/
tags: Experimental Mathematics, Seminar, Math Philosophy and History

En tant que chercheuse en combinatoire algébrique, l’approche exploratoire est essentielle à mon travail. Mon processus de recherche est un va et vient constant entre des expérimentations manuelles (calculs, énumération exhaustives de structures, cas particuliers, etc) et sur ordinateur. Dans cet exposé, je me baserai sur un problème en cours pour dérouler le processus de recherche qui m’amène à obtenir de nouveaux résultats.

