Title: Combinatoire : compter et programmer des objets mathématiques
date: 2020-05-29 00:00
slug: 2019-05-29-filles-et-maths
lang: fr
location: Online
conference_name: Journée Filles et Maths
tags: Filles et Maths,  Science popularization, Video
demo_ext_link: https://github.com/VivianePons/public-notebooks/blob/master/2020-05-29-FillesEtMaths.ipynb
video: https://www.twitch.tv/videos/645825261
slides: 2020_filles-et-maths.pdf


"Combien de façons ai-je d’empiler mes 3 pulls sur l’étagère ? Et si j’ai 150 pulls, tous sur une unique très grande pile ?"
 
Voilà des questions de combinatoire, cette branche des mathématiques qui aime compter les choses : les pulls, les tirages du loto, les chemins dans le plan, les façons de découper un polygone, etc.
 
Et quand on ne connaît pas la réponse, on demande à l’ordinateur de nous aider ! Comment ? Grâce à python.

