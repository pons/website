Title: Combinatoire : compter et programmer des objets mathématiques
date: 2021-03-25 00:00
slug: 2019-03-25-festival-laths
lang: fr
location: Online
conference_name: Festival Maths en Scène
tags: Science popularization
demo_ext_link: https://github.com/VivianePons/public-notebooks/blob/master/2020-05-29-FillesEtMaths.ipynb
slides: 2021_festival.pdf


"Combien de façons ai-je d’empiler mes 3 pulls sur l’étagère ? Et si j’ai 150 pulls, tous sur une unique très grande pile ?"
 
Voilà des questions de combinatoire, cette branche des mathématiques qui aime compter les choses : les pulls, les tirages du loto, les chemins dans le plan, les façons de découper un polygone, etc.
 
Et quand on ne connaît pas la réponse, on demande à l’ordinateur de nous aider ! Comment ? Grâce à python.

