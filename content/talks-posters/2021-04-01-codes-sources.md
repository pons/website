Title: Le notebook Jupyter : un outil pour l'enseignement
date: 2021-04-01 00:10
slug: 2021-04-01-codes-sources
lang: fr
location: Online
slides: 2021_codes-sources.pdf
conference_name: Séminaire Codes Sources
tags: Jupyter


Jupyter est un outil qui permet d’afficher à la fois du texte et des
  cellules de code éditables et exécutables, le tout dans un navigateur
  web. Lors de cet exposé, nous présentons le fonctionnement général de
  l’outil, son utilisation dans un cadre pédagogique, et les solutions
  pour le mettre en place. Nous verrons en particulier comment il est
  utilisé à différent niveaux et pour différents langages de
  programmation à l’université Paris-Saclay pour l’enseignement de
  l’informatique. 
