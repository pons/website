Title: Permutree Sorting
date: 2022-03-31 00:10
slug: 2022-03-31-colombie
lang: en
location: Online
slides: 2022_colombie.pdf
conference_name: Seminario Sabanero de Combinatoria
tags: Permutrees, Seminar


 Permutrees define combinatorial families interpolating between permutations, binary trees and binary sequences. They also correspond to certain congruence classes of the weak order lattice on permutations.  In this talk, we present the Permutree sorting algorithm which attempts to sort permutations following certain constraints, succeeding only when the permutation is minimal inside its permutree congruence class. In this sense, it is a generalization of the well known stack sorting from Knuth and the c-sorting related to Cambrian lattices defined by Reading.
(joint work with D. Tamayo and V. Pilaud)
