Title: Permutrees
date: 2018-05-25 00:10
slug: 2018-05-25-permutrees-talk
lang: en
location: Heraklion
conference_name: Seminar
slides: 2018_Permutrees_Heraklion.pdf
tags: Seminar, Permutrees


The permutrees are a family of combinatorial objects which includes permutations, 
binary trees, binary sequences and a range of objects "in between". In this talk,
we will mostly discuss their combinatorial properties, especially the lattice structure.
We will also show the polytopes that can be constructed from this combinatorics 
which include the permutaheadron and associahedron. 
