Title: Presenting the multipolynomial bases package
date: 2020-10-29
slug: 2020-10-20-polynomials
lang: en
location: Online
conference_name: Sage Days 110
conference_url: https://wiki.sagemath.org/days110
demo_ext_link: https://github.com/VivianePons/multipolynomial-bases/blob/master/examples/2020-10-SageDaysPresentation.ipynb
tags: Sage, Multivariate polynomials


In this talk, we present an external Sage package to work on multivariate polynomials seen as an algebra over integer vectors (the exponents). This allows for manipulation of divided differences operators and the definition of many bases of multivariate polynomials such as the Schubert polynomials, Grothendieck, and Demazure Characters.

