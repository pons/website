Title: SageMath: research and experimentation in Combinatorics
date: 2024-09-06 08:00
slug: 2024-09-06-talk
lang: en
location: ICTM (Online)
tags: Sage, Associahedron, Permutohedron
demo_ext_link: https://github.com/VivianePons/2024_Mumbai/

The course presents the mathematical software SageMath and most specifically its usage for research in combinatorics. We will focus on families of combinatorial objects, especially related
to the Tamari lattice, and their implementation in the context of object oriented programming.


