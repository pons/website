Title: Bases of multivariate polynomials
date: 2021-02-19
slug: 2021-02-19-icerm
lang: en
location: ICERM, Online
conference_name: Sage/Oscar Days for Combinatorial Algebraic Geometry
conference_url: https://icerm.brown.edu/programs/sp-s21/w2/
demo_ext_link: https://github.com/VivianePons/public-notebooks/blob/master/2021-02-ICERMPresentation.ipynb
tags: ICERM, Multivariate Polynomials, Sage


