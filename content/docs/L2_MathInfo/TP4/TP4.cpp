#include <vector>
#include <iostream>
using namespace std;

/** Infrastructure minimale de test **/
#define ASSERT(test) if (!(test)) cout << "Test failed in file " << __FILE__ << " line " << __LINE__ << ": " #test << endl;

/******************************************/
/**** Exercice 1 : Comptage de lignes  ****/
/******************************************/

/** Retourne le nombre lignes possibles formées de briques de tailles 2 et 3
 * et calculé de façon récursive
 * @param n un entier
 * @return le nombre de lignes possible
 **/
int nbLignesRec(int n) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction nbLignesRec
 **/
void testNbLignesRec() {
    cout << "Début des tests : nb lignes rec" << endl;
    ASSERT(nbLignesRec(0) == 1);
    ASSERT(nbLignesRec(-5) == 0);
    ASSERT(nbLignesRec(1) == 0);
    ASSERT(nbLignesRec(2) == 1);
    ASSERT(nbLignesRec(3) == 1);
    ASSERT(nbLignesRec(4) == 1);
    ASSERT(nbLignesRec(5) == 2);
    ASSERT(nbLignesRec(6) == 2);
    ASSERT(nbLignesRec(7) == 3);
    ASSERT(nbLignesRec(8) == 4);
    ASSERT(nbLignesRec(9) == 5);
    ASSERT(nbLignesRec(10) == 7);
    ASSERT(nbLignesRec(11) == 9);
    ASSERT(nbLignesRec(12) == 12);
    cout << "Fin des tests : nb lignes rec" << endl;

}

/** Retourne le nombre lignes possibles formées de briques de tailles 2 et 3
 * et calculé de façon dynamique
 * @param n un entier
 * @return le nombre de lignes possible
 **/
int nbLignesDyn(int n) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}


/** Test de la fonction nbLignesDyn
 **/
void testNbLignesDyn() {
    cout << "Début des tests : nb lignes dyn" << endl;
    ASSERT(nbLignesDyn(0) == 1);
    ASSERT(nbLignesDyn(-5) == 0);
    ASSERT(nbLignesDyn(1) == 0);
    ASSERT(nbLignesDyn(2) == 1);
    ASSERT(nbLignesDyn(3) == 1);
    ASSERT(nbLignesDyn(4) == 1);
    ASSERT(nbLignesDyn(5) == 2);
    ASSERT(nbLignesDyn(6) == 2);
    ASSERT(nbLignesDyn(7) == 3);
    ASSERT(nbLignesDyn(8) == 4);
    ASSERT(nbLignesDyn(9) == 5);
    ASSERT(nbLignesDyn(10) == 7);
    ASSERT(nbLignesDyn(11) == 9);
    ASSERT(nbLignesDyn(12) == 12);
    ASSERT(nbLignesDyn(40) == 31572);
    cout << "Fin des tests : nb lignes dyn" << endl;

}

/**************************************/
/**** Exercice 2 : Lignes et murs  ****/
/**************************************/

typedef vector<bool> ligne;
typedef vector<ligne> mur;

ligne exempleLigne = {true, false, false, true, false, true, false, false, true};
ligne exempleLigne2 = {true, false, false, true, false, false, true, false, false, true};
ligne contreExemple1 = {false, true, false, true, false, true, false, false, true};
ligne contreExemple2 = {true, false, false, true, false, true, false, true, false};
ligne contreExemple3 = {true, false, false, false, false, true, false, false, true};
ligne contreExemple4 = {true, false, false, true, false, true, true, false, true};

mur exempleMur = {
    {true,  false, false, true,  false, true,  false, false, true},
    {true,  false, true,  false, true,  false, true,  false, true},
    {true,  false, false, true,  false, true,  false, false, true}
};

mur exempleMur2 = {
    {true,  false, true,  false, false, true,  false, true,  false, true},
    {true,  false, false, true,  false, false, true,  false, false, true},
    {true,  false, true,  false, true,  false, false, true,  false, true}
};

mur contreExempleMur1 = {
    {true,  false, false, true,  false, true,  false, false, true},
    {true, false, false, true, false, true, false, true, false},
    {true,  false, false, true,  false, true,  false, false, true}
};

mur contreExempleMur2 = {
    {true,  false, false, true,  false, true,  false, true},
    {true,  false, true,  false, true,  false, true,  false, true},
    {true,  false, false, true,  false, true,  false, false, true}
};

mur contreExempleMur3 = {
    {true,  false, false, true,  false, true,  false, false, true},
    {true,  false, true,  false, true,  false, false,  true},
    {true,  false, false, true,  false, true,  false, false, true}
};

mur contreExempleMur4 = {
    {true,  false, false, true,  false, true,  false, false, true},
    {true,  false, true,  false, false, true,  false, false, true},
    {true,  false, false, true,  false, false, true,  false, true}
};


/** Affiche une ligne de brique sur la sortie standard
 **/
void afficheLigne(const ligne &L) {
    for(unsigned int i = 0; i < L.size() - 1; i++) {
        if(L[i]) cout << "|";
        else cout << " ";
        cout << "_";
    }
    cout << "|" << endl;
}


/** Affiche un mur de brique sur la sortie standard
 **/
void afficheMur(const mur &M) {
    if (M.size() > 0) {
        unsigned int n = M[0].size();
        for(unsigned int i = 0; i < n-1; i++) cout << " _"; // printing the top empty line
        cout << endl;
        for(unsigned int i = 0; i < M.size(); i++) afficheLigne(M[i]);
    }
}

/** Teste si un tableau de booléen représente bien une ligne de briques
 * (tailles 2 et 3)
 * @param L, un tableau de booléens
 * @return true si la ligne est bien formée, false sinon
 **/
bool estLigne(const ligne &L) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estLigne
 **/
void testEstLigne() {
    cout << "Début des tests : est ligne" << endl;
    ASSERT(not estLigne({}));
    ASSERT(estLigne({true}));
    ASSERT(estLigne({true,false,true}));
    ASSERT(estLigne({true,false,false,true}));
    ASSERT(not estLigne({true,true,true}));
    ASSERT(not estLigne({true,false, false, false, true}));
    ASSERT(estLigne(exempleLigne));
    ASSERT(not estLigne(contreExemple1));
    ASSERT(not estLigne(contreExemple2));
    ASSERT(not estLigne(contreExemple3));
    ASSERT(not estLigne(contreExemple4));
    cout << "Fin des tests : est ligne" << endl;
}

/** Teste si le mur est bien composé de ligne de briques qui suivent nos conventions
 * chaque ligne doit vérifier la fonction estLigne
 * et les lignes doivent être de même taille
 * cependant, on ne vérifie pas les jointures
 * @param m, un objet de type mur
 * @return true si toutes les lignes du mur sont bien formées et sont de même taille
 **/
bool formatLignesMur(const mur &m) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction formatLignesMur
 **/
void testFormatLignesMur() {
    cout << "Début des tests : format lignes mur" << endl;
    ASSERT(formatLignesMur(exempleMur));
    ASSERT(formatLignesMur(exempleMur2));
    ASSERT(formatLignesMur(contreExempleMur4));
    ASSERT(not formatLignesMur(contreExempleMur1));
    ASSERT(not formatLignesMur(contreExempleMur2));
    ASSERT(not formatLignesMur(contreExempleMur3));
    cout << "Fin des tests : format lignes mur" << endl;
}

/** Teste si les jointures de deux lignes sont bien non alignées
 * @param L1 une ligne de brique
 * @param L2 une ligne de brique
 * @return true si les jointures de L1 et L2 sont à des positions disctinctes
 * L1 et L2 doivent être de même taille (sinon la fonction renvoie false)
 **/
bool jointuresDistinctes(const ligne &L1,const ligne &L2) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction jointuresDistinctes
 **/
void testJointuresDisctinctes() {
    cout << "Début des tests : jointures disctinces" << endl;
    ASSERT(jointuresDistinctes({true}, {true}));
    ASSERT(jointuresDistinctes({true, false, true}, {true, false, true}));
    ASSERT(jointuresDistinctes({true, false, false, true}, {true, false, false, true}));
    ASSERT(jointuresDistinctes(exempleMur[0], exempleMur[1]));
    ASSERT(jointuresDistinctes({true, false, true, false, false, true}, {true, false, false, true, false, true}));
    ASSERT( not jointuresDistinctes({true, false, true, false, true}, {true, false, true, false, true}));
    ASSERT(jointuresDistinctes({true, false, true, false, true, false, true}, {true, false, false, true, false, false, true}));
    ASSERT(not jointuresDistinctes({true, false, true, false, true, false, true, false, true}, {true, false, false, true, false, false, true, false, true}));
    cout << "Fin des tests : jointures disctinces" << endl;
}


/** Teste si les jointures des différentes lignes du mur sont non alignées
 * le format des lignes est supposé correct
 * @param m, un objet de type mur
 * @return true si les jointures sont non allignées
 **/
bool jointuresLignesMur(const mur &m) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction jointuresLignesMur
 **/
void testJointuresLignesMur() {
    cout << "Début des tests : jointures lignes mur" << endl;
    ASSERT(jointuresLignesMur(exempleMur));
    ASSERT(jointuresLignesMur(exempleMur2));
    ASSERT(not jointuresLignesMur(contreExempleMur4));
    cout << "Fin des tests : jointures lignes mur" << endl;
}

/********************************************/
/**** Exercice 3 : Génération de lignes  ****/
/********************************************/

/** Génération de vecteurs de boolééen de taille k
 * @param k, un entier
 * @return une collection de vecteurs de booléens
 **/
vector<vector<bool>> genereBooleens(int k) {
    vector<vector<bool>> collectionk = vector<vector<bool>>();
    if (k < 0) {
        // Retourner la collection vide
        return collectionk;
    }
    if (k == 0) {
        // Retourner la collection contenant le vecteur de taille 0
        collectionk.push_back(vector<bool>());
        return collectionk;
    }
    vector<vector<bool>> collectionprev = genereBooleens(k-1); // on récupère les vecteurs de taille k-1
    for(unsigned int i = 0; i < collectionprev.size(); i++) {
        // on effectue 2 copies de l'élément i
        vector<bool> v0 = collectionprev[i];
        vector<bool> v1 = collectionprev[i];
        // rajouter false à v0 et l'ajouter à collectionk
        v0.push_back(false);
        collectionk.push_back(v0);
        // rajouter true à v1 et l'ajouter à collectionk
        v1.push_back(true);
        collectionk.push_back(v1);
    }
    return collectionk;
}

/** Test de la fonction genereBooleens
 **/
void testGenereBooleens() {
    cout << "Début des tests : genere booleens" << endl;
    ASSERT(genereBooleens(0).size() == 1);
    ASSERT(genereBooleens(-2).size() == 0);
    ASSERT(genereBooleens(1).size() == 2);
    ASSERT(genereBooleens(1)[0].size() == 1);
    ASSERT(genereBooleens(2).size() == 4);
    ASSERT(genereBooleens(3).size() == 8);
    ASSERT(genereBooleens(3)[2].size() == 3);
    cout << "Fin des tests : genere booleens" << endl;
}

/** Retourne l'ensemble des lignes de taille k
 * en utilisant genereBooleens
 * @param k, la taille des lignes
 * @return un ensemble de lignes
 **/
vector<ligne> genereLignesMethode1(int k) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction genereLignesMethode1
 **/
void testGenereLignesMethode1() {
    cout << "Début des tests : genere lignes, méthode 1" << endl;
    ASSERT(genereLignesMethode1(-2).size() == 0);
    ASSERT(genereLignesMethode1(0).size() == 1);
    ASSERT(genereLignesMethode1(1).size() == 0);
    ASSERT(genereLignesMethode1(2).size() == 1);
    ASSERT(genereLignesMethode1(3).size() == 1);
    ASSERT(genereLignesMethode1(4).size() == 1);
    cout << "Fin des tests : genere lignes, méthode 1" << endl;
}

/** Ajoute une brique de taille 2 à la ligne l
 * @param une ligne l passée par référence (la ligne sera modifiée par la fonction)
 **/
void ajouteBrique2(ligne &l) {
    l.push_back(false);
    l.push_back(true);
}

/** Test de la fonction ajouteBrique2
 **/
void testAjouteBrique2() {
    cout << "Début des tests : ajoute brique 2" << endl;
    ligne l = {true};
    ligne l2 = {true, false, true};
    ligne l3 = {true, false, true, false, true};
    ajouteBrique2(l);
    ASSERT(l == l2);
    ajouteBrique2(l);
    ASSERT(l == l3);
    cout << "Fin des tests : ajoute brique 2" << endl;
}

/** Ajoute une brique de taille 3 à la ligne l
 * @param une ligne l passée par référence (la ligne sera modifiée par la fonction)
 **/
void ajouteBrique3(ligne &l) {
    l.push_back(false);
    l.push_back(false);
    l.push_back(true);
}

/** Test de la fonction ajouteBrique3
 **/
void testAjouteBrique3() {
    cout << "Début des tests : ajoute brique 3" << endl;
    ligne l = {true};
    ligne l2 = {true, false, false, true};
    ligne l3 = {true, false, false, true, false, false, true};
    ajouteBrique3(l);
    ASSERT(l == l2);
    ajouteBrique3(l);
    ASSERT(l == l3);
    cout << "Fin des tests : ajoute brique 3" << endl;
}

/** Retourne l'ensemble des lignes de taille k
 * en utilisant la structure des lignes
 * @param k, la taille des lignes
 * @return une collection de lignes
 **/
vector<ligne> genereLignesMethode2(int k) {
    vector<ligne> lignes = vector<ligne>(); // Collection "reponse"
    if(k < 0) {
        // Retourner la collection vide

    }
    if(k == 0) {
        // Retourner la collection contenant la ligne vide

    }
    // Mettre dans ligne 2 l'ensemble des lignes de taille k-2
    vector<ligne> lignes2;

    for(unsigned int i = 0; i < lignes2.size(); i++) {
        ligne l = lignes2[i]; // on copie l'élément i
        // Rajouter une brique de taille 2

        // Rajouter la ligne à la collection

    }
    // Mettre dans ligne 2 l'ensemble des lignes de taille k-3
    vector<ligne> lignes3;
    for(unsigned int i = 0; i < lignes3.size(); i++) {
        ligne l = lignes3[i]; // on copie l'élément i
        // Rajouter une brique de taille 3

        // Rajouter la ligne à la collection

    }
    return lignes;
}

/** Test de la fonction genereLignesMethode2
 **/
void testGenereLignesMethode2() {
    cout << "Début des tests : genere lignes, méthode 2" << endl;
    ASSERT(genereLignesMethode2(-2).size() == 0);
    ASSERT(genereLignesMethode2(0).size() == 1);
    ASSERT(genereLignesMethode2(1).size() == 0);
    ASSERT(genereLignesMethode2(2).size() == 1);
    ASSERT(genereLignesMethode2(3).size() == 1);
    ASSERT(genereLignesMethode2(4).size() == 1);
    vector<ligne> L5 = genereLignesMethode2(5);
    ASSERT(L5.size() == 2);
    ASSERT(L5[0].size() == 6);
    ASSERT(L5[1].size() == 6);
    ASSERT(L5[0][0]);
    ASSERT(L5[0][5]);
    ASSERT(L5[1][0]);
    ASSERT(L5[1][5]);
    vector<ligne> L10 = genereLignesMethode2(10);
    unsigned int s = nbLignesDyn(10);
    ASSERT(L10.size() == s);
    ASSERT(L10[0].size() == 11);
    afficheMur(L10); // affiche toutes les lignes de taille 10
    cout << "Fin des tests : genere lignes, méthode 2" << endl;
}

/******************************************/
/**** Exercice 4 : Génération de murs  ****/
/******************************************/


/** Retourne l'ensemble des murs de hauteur n et longueur k
 * @param n, la hauteur du mur
 * @param k, la taille des lignes
 * @return une collection de murs
 **/
vector<mur> genereMurs(int n, int k) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne

}

/** Test de la fonction genereMurs
 **/
void testGenereMurs() {
    cout << "Début des tests : genere murs" << endl;
    vector<mur> M = genereMurs(3,6);
    ASSERT(M.size() == 2);
    for(unsigned int i =0; i < M.size(); i++) {
        ASSERT(M[i].size() == 3);
        ASSERT(formatLignesMur(M[i]));
        ASSERT(jointuresLignesMur(M[i]));
        afficheMur(M[i]);
        cout << endl;
    }
    M = genereMurs(3,9);
    ASSERT(M.size() == 8);
    for(unsigned int i =0; i < M.size(); i++) {
        ASSERT(M[i].size() == 3);
        ASSERT(formatLignesMur(M[i]));
        ASSERT(jointuresLignesMur(M[i]));
        afficheMur(M[i]);
        cout << endl;
    }
    ASSERT(genereMurs(-2,3).size() == 0);
    M = genereMurs(0,2);
    ASSERT(M.size() == 1);
    ASSERT(M[0].size() == 0);
    M = genereMurs(3,0);
    ASSERT(M.size() == 1);
    ASSERT(M[0].size() == 3);
    ASSERT(M[0][0].size() == 1);
    cout << "Fin des tests : genere murs" << endl;
}

/** Retourne l'ensemble des lignes de taille k telles que les jointures soient distinctes de prev
 * @param k, la taille des lignes
 * @param prev, la ligne précédente du mur, qu'on suppose de taille >= k
 * @return une collection de lignes
 **/
vector<ligne> genereLignesMethode3(int k, const ligne &prev) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

void testGenereLignesMethode3() {
    cout << "Début des tests : genere lignes, méthode 3" << endl;
    ASSERT(genereLignesMethode3(-2,exempleLigne).size() == 0);
    ASSERT(genereLignesMethode3(0,exempleLigne).size() == 1);
    ASSERT(genereLignesMethode3(1,exempleLigne).size() == 0);
    ASSERT(genereLignesMethode3(2,exempleLigne).size() == 1);
    ASSERT(genereLignesMethode3(3,exempleLigne).size() == 0);
    ASSERT(genereLignesMethode3(4,exempleLigne).size() == 1);
    vector<ligne> L = genereLignesMethode3(8, exempleLigne);
    ASSERT(L.size() == 1);
    ASSERT(jointuresDistinctes(L[0], exempleLigne));
    L = genereLignesMethode3(9, exempleLigne2);
    ASSERT(L.size() == 2);
    ASSERT(jointuresDistinctes(L[0], exempleLigne2));
    ASSERT(jointuresDistinctes(L[1], exempleLigne2));
    cout << "Fin des tests : genere lignes, méthode 3" << endl;

}

/** Retourne l'ensemble des murs de hauteur n et longueur k
 * en utilisant la fonction genereLignesMethdoe3
 * @param n, la hauteur du mur
 * @param k, la taille des lignes
 * @return une collection de murs
 **/
vector<mur> genereMursMethode2(int n, int k) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction genereMursMethode2
 **/
void testGenereMursMethode2() {
    cout << "Début des tests : genere murs methode 2" << endl;
    vector<mur> M = genereMursMethode2(3,6);
    ASSERT(M.size() == 2);
    for(unsigned int i =0; i < M.size(); i++) {
        ASSERT(M[i].size() == 3);
        ASSERT(formatLignesMur(M[i]));
        ASSERT(jointuresLignesMur(M[i]));
        afficheMur(M[i]);
        cout << endl;
    }
    M = genereMursMethode2(3,9);
    ASSERT(M.size() == 8);
    for(unsigned int i =0; i < M.size(); i++) {
        ASSERT(M[i].size() == 3);
        ASSERT(formatLignesMur(M[i]));
        ASSERT(jointuresLignesMur(M[i]));
        afficheMur(M[i]);
        cout << endl;
    }
    ASSERT(genereMursMethode2(-2,3).size() == 0);
    M = genereMursMethode2(0,2);
    ASSERT(M.size() == 1);
    ASSERT(M[0].size() == 0);
    M = genereMursMethode2(3,0);
    ASSERT(M.size() == 1);
    ASSERT(M[0].size() == 3);
    ASSERT(M[0][0].size() == 1);
    cout << "Fin des tests : genere murs methdoe 2" << endl;
}

/** Test de la fonction genereLignesMethode3
 **/

int main() {
    // testNbLignesRec();
    // testNbLignesDyn();
    // afficheLigne(exempleLigne);
    // cout << endl;
    // afficheMur(exempleMur);
    // cout << endl;
    // afficheMur(exempleMur2);
    // cout << endl;
    // testEstLigne();
    // testJointuresDisctinctes();
    // testFormatLignesMur();
    // testJointuresLignesMur();
    // testGenereBooleens();
    // testGenereLignesMethode1();
    // testAjouteBrique2();
    // testAjouteBrique3();
    // testGenereLignesMethode2();
    // testGenereMurs();
    // testGenereLignesMethode3();
    // testGenereMursMethode2();
}
