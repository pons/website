#include <vector>
#include <iostream>
using namespace std;

/** Infrastructure minimale de test **/
#define ASSERT(test) if (!(test)) cout << "Test failed in file " << __FILE__ << " line " << __LINE__ << ": " #test << endl;

typedef vector<vector<bool>> Relation; /* on définit Relation en tant que nouveau type (double tableau de bool) */

/* Relations exemples */

/* diagonale 3 */
Relation ex1 = {{true, false, false},
                {false, true, false},
                {false, false, true}};

/* relation i<=j */
Relation ex2 = {{true, true, true, true, true},
                {false, true, true, true, true},
                {false, false, true, true, true},
                {false, false, false, true, true},
                {false, false, false, false, true}};

/* relation i est diviseur de j */
Relation ex3 = {{false, false, false, false, false},
                {true, true, true, true, true},
                {true, false, true, false, true},
                {true, false, false, true, false},
                {true, false, false, false, true}};

/* relation i=j mod 4 */
Relation ex4 = {{true, false, false, false, true},
                {false, true, false, false, false},
                {false, false, true, false, false},
                {false, false, false, true, false},
                {true, false, false, false, true}};

/* relation j = i+1*/
Relation ex5 = {{false, true, false, false, false},
                {false, false, true, false, false},
                {false, false, false, true, false},
                {false, false, false, false, true},
                {false, false, false, false, false}};

/* relation i >= j*/
Relation ex6 = {{true, false, false, false, false},
                {true, true, false, false, false},
                {true, true, true, false, false},
                {true, true, true, true, false},
                {true, true, true, true, true}};

/* union ex3 ex5 */
Relation ex7 = {{false, true, false, false, false},
                {true, true, true, true, true},
                {true, false, true, true, true},
                {true, false, false, true, true},
                {true, false, false, false, true}};

/* intersection ex3 ex5 */
Relation ex8 = {{false, false, false, false, false},
                {false, false, true, false, false},
                {false, false, false, false, false},
                {false, false, false, false, false},
                {false, false, false, false, false}};

/* inverse ex4 */
Relation ex9 = {{false, true, true, true, false},
                {true, false, true, true, true},
                {true, true, false, true, true},
                {true, true, true, false, true},
                {false, true, true, true, false}};

/* composition ex4 ex5 */
Relation ex10 = {{false, true, false, false, false},
                 {false, false, true, false, false},
                 {false, false, false, true, false},
                 {false, false, false, false, true},
                 {false, true, false, false, false}};



/* Création de relations */

/** Retourne une relation vide de 0...n-1 sur 0...n-1
 * @param n, la taille de l'ensemble
 * @return une Relation vide (sous forme de matrice booléenne initialisée à false) sur un ensemble de taille n
 **/
Relation emptyRelation(int n) {
    return Relation(n, vector<bool>(n,false));
}

/** Retourne une relation pleine de 0...n-1 sur 0...n-1
 * @param n, la taille de l'ensemble
 * @return une Relation pleine (sous forme de matrice booléenne initialisée à true) sur un ensemble de taille n
 **/
Relation fullRelation(int n) {
    return Relation(n, vector<bool>(n,true));
}

/** Retourne une relation diagonale de 0...n-1 sur 0...n-1
 * @param n, la taille de l'ensemble
 * @return une Relation diagonale sur un ensemble de taille n
 **/
Relation diagonalRelation(int n) {
    Relation R = emptyRelation(n);
    for(int i=0; i < n; i++) {
        R[i][i] = true;
    }
    return R;
}

/********************************************/
/**** Exercice 1 :Affichage et exemples  ****/
/********************************************/


/** Affiche la matrice de la relation sous forme de 0 et de 1
 * @param R, une relation
 **/
void afficheMatriceRelation(const Relation &R) {
    for(unsigned int i =0; i < R.size(); i++) {
        for(unsigned int j=0; j < R[i].size(); j++) {
            cout << R[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

/** Test de la fonction afficheMatriceRelation
 */
void testAfficheMatriceRelation() {
    afficheMatriceRelation(emptyRelation(3)); /* affiche un carré de 0 de 3 par 3 */
    afficheMatriceRelation(emptyRelation(0)); /* relation de 2 ensembles vides */
    afficheMatriceRelation(ex1); /* une diagonale de 1 */
    afficheMatriceRelation(ex2);
    afficheMatriceRelation(ex3);
    afficheMatriceRelation(ex4);
    afficheMatriceRelation(ex5);
}

/** Affiche les couples (i,j) qui appartiennent à la relation (R[i][j] est vrai)
 * @param R, une relation
 **/
void afficheCouplesRelation(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction afficheCouplesRelation
 */
void testAfficheCouplesRelation() {
    afficheCouplesRelation(emptyRelation(3)); /* relation vide */
    afficheCouplesRelation(emptyRelation(0)); /* relation de 2 ensembles vides */
    afficheCouplesRelation(ex1); /* 3 couples */
    afficheCouplesRelation(ex2); /* 15 couples */
    afficheCouplesRelation(ex3); /* 12 couples */
    afficheCouplesRelation(ex4); /* 7 couples */
    afficheCouplesRelation(ex5); /* 4 couples */
}

/** Crée une relation exemple dépendant d'un paramètre k
 * R(i,j) ssi i%k <= j%k et i/k <= j/k (division entière)
 * @param n, la taille de l'ensemble
 * @param k, un entier
 * @return la relation décrite
 **/
Relation exemplek(int n, int k) {
    Relation R = emptyRelation(n);
    for(int i = 0; i < n; i++) {
        for(int j =0; j < n; j++) {
            if(i%k <= j%k and i/k <= j/k) {
                R[i][j] = true;
            }
        }
    }
    return R;
}



/** Crée la relation "plus petit ou égal" sur l'ensemble {0, ..., n-1}
 * @param n, la taille de l'ensemble
 * @return la relation "plus petit ou égale"
 **/
Relation plusPetitEgal(int n) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction plusPetitEgal
 **/
void testPlusPetitEgal() {
    cout << "Début des tests : plus petit ou egal" << endl;
    ASSERT(plusPetitEgal(5) == ex2);
    ASSERT(plusPetitEgal(0) == vector<vector<bool>>());
    Relation R = plusPetitEgal(100);
    ASSERT(R[42][42]);
    ASSERT(R[42][85]);
    ASSERT(R[42][99]);
    ASSERT(R[0][42]);
    ASSERT(not R[42][0]);
    ASSERT(not R[42][35]);
    ASSERT(not R[99][42]);
    cout << "Fin des tests : plus petit ou egal" << endl;
}

/** Crée la relation "est diviseur de" sur l'ensemble {0, ..., n-1}
 * @param n, la taille de l'ensemble
 * @return la relation "est diviseur"
 **/
Relation diviseur(int n) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction diviseur
 **/
void testDiviseur() {
    cout << "Début des tests : diviseur" << endl;
    ASSERT(diviseur(5) == ex3);
    ASSERT(diviseur(0) == vector<vector<bool>>());
    Relation R = diviseur(100);
    ASSERT(R[42][42]);
    ASSERT(R[42][84]);
    ASSERT(not R[42][99]);
    ASSERT(not R[0][42]);
    ASSERT(R[1][42]);
    ASSERT(R[2][42]);
    ASSERT(R[3][42]);
    ASSERT(R[7][42]);
    ASSERT(R[6][42]);
    ASSERT(R[21][42]);
    ASSERT(not R[5][42]);
    ASSERT(R[42][0]);
    ASSERT(not R[42][35]);
    ASSERT(not R[99][42]);
    cout << "Fin des tests : diviseur" << endl;
}

/** Crée la relation "même reste par k" sur l'ensemble {0, ..., n-1}
 * @param n, la taille de l'ensemble
 * @param k, un entier positif (on suppose que c'est le cas, pas besoin de tester)
 * @return la relation "même reste mod k"
 **/
Relation modk(int n, int k) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction mod4
 **/
void testModK() {
    cout << "Début des tests : mod4" << endl;
    ASSERT(modk(5,4) == ex4);
    ASSERT(modk(0,0) == vector<vector<bool>>());
    Relation R = modk(100,4);
    ASSERT(R[42][42]);
    ASSERT(not R[42][85]);
    ASSERT(not R[42][99]);
    ASSERT(not R[0][42]);
    ASSERT(not R[42][0]);
    ASSERT(not R[42][35]);
    ASSERT(not R[99][42]);
    ASSERT(R[0][4]);
    ASSERT(R[40][80]);
    ASSERT(R[42][6]);
    ASSERT(R[43][47]);
    ASSERT(R[45][53]);
    R = modk(10,3);
    ASSERT(R[0][3]);
    ASSERT(R[3][6]);
    ASSERT(R[9][6]);
    ASSERT(R[0][9]);
    ASSERT(not R[3][1]);
    ASSERT(R[1][4]);
    ASSERT(R[7][4]);
    ASSERT(R[1][7]);
    ASSERT(not R[5][1]);
    ASSERT(R[2][5]);
    ASSERT(R[8][5]);
    ASSERT(R[2][5]);
    cout << "Fin des tests : mod4" << endl;
}

/*******************************************/
/**** Exercice 2 :Premières propriétés  ****/
/*******************************************/

/** Teste l'inclusion de deux relations de même taille
 * @param R1, une relation binaire de taille n
 * @param R2, une relation binaire de taille m
 * @return true si n == m et R1 est inscluse dans R2, false sinon
 * (si les relations sont de tailles différentes, la fonction retourne false)
 **/
bool incluse(const Relation &R1, const Relation &R2) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction incluse
 */
void testIncluse() {
    cout << "Début des tests : incluse" << endl;
    Relation E3 = emptyRelation(3);
    Relation E5 = emptyRelation(5);
    Relation F5 = fullRelation(5);
    ASSERT(incluse(E3,ex1));
    ASSERT(not incluse(E3,ex2)); /* tailles différentes */
    ASSERT(incluse(E5,ex2));
    ASSERT(incluse(E5,ex3));
    ASSERT(incluse(E5,ex4));
    ASSERT(incluse(E5,ex5));
    ASSERT(not incluse(ex2, E5));
    ASSERT(not incluse(ex3, E5));
    ASSERT(not incluse(ex4, E5));
    ASSERT(not incluse(ex5, E5));
    ASSERT(incluse(ex2, F5));
    ASSERT(incluse(ex3, F5));
    ASSERT(incluse(ex4, F5));
    ASSERT(incluse(ex5, F5));
    ASSERT(incluse(ex5,ex2));
    ASSERT(incluse(modk(100,4), modk(100,2)));
    ASSERT(not incluse(plusPetitEgal(100), diviseur(100)));
    ASSERT(incluse(exemplek(100,3),plusPetitEgal(100)));
    cout << "Fin des tests : incluse" << endl;
}


/** Teste de la reflexibilité
 * une relation R est reflexive si R(i,i) pour tout i
 * @param R, une relation
 * @return true si la relation est Reflexive, false sinon
 **/
bool estReflexive(Relation R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estReflexive
 */
void testEstReflexive() {
    cout << "Début des tests : reflexive" << endl;
    ASSERT(estReflexive(fullRelation(3)));
    ASSERT(not estReflexive(emptyRelation(3)));
    ASSERT(estReflexive(ex1));
    ASSERT(estReflexive(ex2));
    ASSERT(estReflexive(ex4));
    ASSERT(not estReflexive(ex3));
    ASSERT(not estReflexive(ex5));
    ASSERT(estReflexive(modk(100,4)));
    ASSERT(estReflexive(plusPetitEgal(100)));
    ASSERT(not estReflexive(diviseur(100)));
    ASSERT(estReflexive(exemplek(100,4)));
    cout << "Fin des tests : reflexive" << endl;
}

/** Teste de la symétrie
 * une relation R est symétrique si R(i,j) <=> R(j,i) pour tout i,j
 * @param R, une relation
 * @return true si la relation est symétrique, false sinon
 **/
bool estSymetrique(Relation R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estSymetrique
 */
void testEstSymetrique() {
    cout << "Début des tests : symetrique" << endl;
    ASSERT(estSymetrique(fullRelation(3)));
    ASSERT(estSymetrique(emptyRelation(3)));
    ASSERT(estSymetrique(ex1));
    ASSERT(estSymetrique(ex4));
    ASSERT(not estSymetrique(ex2));
    ASSERT(not estSymetrique(ex3));
    ASSERT(not estSymetrique(ex5));
    ASSERT(not estSymetrique(ex7));
    ASSERT(estSymetrique(modk(100,5)));
    ASSERT(estSymetrique(modk(100,3)));
    ASSERT(not estSymetrique(plusPetitEgal(100)));
    ASSERT(not estSymetrique(diviseur(100)));
    ASSERT(not estSymetrique(exemplek(100,4)));
    cout << "Fin des tests : symetrique" << endl;
}

/** Teste de l'anti-symétrie
 * une relation R est anti-symétrique si R(i,j) => (non R(j,i)) pour tout i != j
 * @param R, une relation
 * @return true si la relation est anti-symétrique, false sinon
 **/
bool estAntiSymetrique(Relation R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estAntiSymetrique
 */
void testEstAntiSymetrique() {
    cout << "Début des tests : anti-symetrique" << endl;
    ASSERT(not estAntiSymetrique(fullRelation(3)));
    ASSERT(estAntiSymetrique(emptyRelation(3)));
    ASSERT(estAntiSymetrique(ex1));
    ASSERT(estAntiSymetrique(ex2));
    ASSERT(estAntiSymetrique(ex3));
    ASSERT(estAntiSymetrique(ex6));
    ASSERT(estAntiSymetrique(ex5));
    ASSERT(not estAntiSymetrique(ex4));
    ASSERT(not estAntiSymetrique(ex7));
    ASSERT(not estAntiSymetrique(modk(100,5)));
    ASSERT(not estAntiSymetrique(modk(100,3)));
    ASSERT(estAntiSymetrique(plusPetitEgal(100)));
    ASSERT(estAntiSymetrique(diviseur(100)));
    ASSERT(estAntiSymetrique(exemplek(100,4)));
    cout << "Fin des tests : anti-symetrique" << endl;
}

/** affiche les proprietes symétrie, anti-symétrie, et relfexive d'une relation
 **/
void afficheProprietesSimplesRelation(const Relation &R) {
    cout << "La relation" << endl;
    afficheCouplesRelation(R);
    if(estReflexive(R)) {
        cout << "est reflexive" << endl;
    } else {
        cout << "n'est pas reflexive" << endl;
    }
    if(estSymetrique(R)) {
        cout << "est symetrique" << endl;
    } else {
        cout << "n'est pas symetrique" << endl;
    }
    if(estAntiSymetrique(R)) {
        cout << "est anti-symetrique" << endl;
    } else {
        cout << "n'est pas anti-symetrique" << endl;
    }
}

/** fonction d'illustration de l'indépendance des propriétés reflexive et symetrique
 **/
void illustrationReflexiveSymetrique() {
    cout << "Les propriétés symétriques et réflexives sont indépendantes l'une de l'autre" << endl;
    cout << endl << "exemple symétrie + reflexive" << endl;
    afficheProprietesSimplesRelation(ex4);
    cout << endl << "exemple non symétrique + reflexive" << endl;
    afficheProprietesSimplesRelation(ex2);
    cout << endl << "exemple symétrie + non reflexive" << endl;
    afficheProprietesSimplesRelation(ex9);
    cout << endl << "exemple non symétrie + non reflexive" << endl;
    afficheProprietesSimplesRelation(ex5);
}

/** fonction d'illustration: non symétrie + non antisymétrique
 **/
void illustrationSymetriqueAntisymetrique() {
    cout << "Il est possible pour une relation de n'être ni symétrique, ni anti-symétrique" << endl;

}

/******************************************/
/**** Exercice 3 :Premières opération  ****/
/******************************************/

/** Retourne la relation inverse
 * @param R1, une relation
 * @return la relation inverse de R1
 **/
Relation inverseRel(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction inverseRel
 **/
void testInverseRel() {
    cout << "Début des tests : inverse" << endl;
    Relation E5 = emptyRelation(5);
    Relation F5 = fullRelation(5);
    ASSERT(inverseRel(E5) == F5);
    ASSERT(inverseRel(F5) == E5);
    Relation E0 = emptyRelation(0);
    Relation F0 = fullRelation(0);
    ASSERT(inverseRel(E0) == E0);
    ASSERT(inverseRel(F0) == F0);
    ASSERT(inverseRel(ex4) == ex9);
    ASSERT(inverseRel(ex9) == ex4);
    cout << "Fin des tests : inverse" << endl;

}


/** Retourne l'union de 2 relations
 * @param R1, une relation de taille n
 * @param R2, une relation de taille n,
 * @return la relation union de R1 et R2
 * (on suppose que R1 et R2 ont la même taille)
 **/
Relation unionRel(const Relation &R1, const Relation &R2) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction unionRel
 **/
void testUnionRel() {
    cout << "Début des tests : union" << endl;
    Relation E5 = emptyRelation(5);
    Relation F5 = fullRelation(5);
    ASSERT(unionRel(E5,E5) == E5);
    ASSERT(unionRel(F5,F5) == F5);
    ASSERT(unionRel(E5,F5) == F5);
    Relation E0 = emptyRelation(0);
    Relation F0 = fullRelation(0);
    ASSERT(unionRel(E0,E0) == E0);
    ASSERT(unionRel(F0,F0) == F0);
    ASSERT(unionRel(ex2,E5) == ex2);
    ASSERT(unionRel(E5,ex2) == ex2);
    ASSERT(unionRel(ex2,F5) == F5);
    ASSERT(unionRel(F5,ex2) == F5);
    ASSERT(unionRel(ex3,E5) == ex3);
    ASSERT(unionRel(E5,ex3) == ex3);
    ASSERT(unionRel(ex3,F5) == F5);
    ASSERT(unionRel(F5,ex3) == F5);
    ASSERT(unionRel(ex2,ex6) == F5);
    ASSERT(unionRel(ex3,ex5) == ex7);
    ASSERT(unionRel(ex5,ex2) == ex2);
    ASSERT(unionRel(inverseRel(ex4),ex4) == F5);
    ASSERT(unionRel(modk(100,4), modk(100,2)) == modk(100,2));
    ASSERT(unionRel(inverseRel(modk(100,2)), modk(100,2)) == fullRelation(100));
    cout << "Fin des tests : union" << endl;

}

/** Retourne l'intersection de 2 relations
 * @param R1, une relation de taille n
 * @param R2, une relation de taille n,
 * @return la relation intersection de R1 et R2
 * (on suppose que R1 et R2 ont la même taille)
 **/
Relation intersectionRel(const Relation &R1, const Relation &R2) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction intersectionRel
 **/
void testIntersectionRel() {
    cout << "Début des tests : intersection" << endl;
    Relation E5 = emptyRelation(5);
    Relation F5 = fullRelation(5);
    ASSERT(intersectionRel(E5,E5) == E5);
    ASSERT(intersectionRel(F5,F5) == F5);
    ASSERT(intersectionRel(E5,F5) == E5);
    Relation E0 = emptyRelation(0);
    Relation F0 = fullRelation(0);
    ASSERT(intersectionRel(E0,E0) == E0);
    ASSERT(intersectionRel(F0,F0) == F0);
    ASSERT(intersectionRel(ex2,E5) == E5);
    ASSERT(intersectionRel(E5,ex2) == E5);
    ASSERT(intersectionRel(ex2,F5) == ex2);
    ASSERT(intersectionRel(F5,ex2) == ex2);
    ASSERT(intersectionRel(ex3,E5) == E5);
    ASSERT(intersectionRel(E5,ex3) == E5);
    ASSERT(intersectionRel(ex3,F5) == ex3);
    ASSERT(intersectionRel(F5,ex3) == ex3);
    ASSERT(intersectionRel(ex2,ex6) == diagonalRelation(5));
    ASSERT(intersectionRel(ex3,ex5) == ex8);
    ASSERT(intersectionRel(ex5,ex2) == ex5);
    ASSERT(intersectionRel(inverseRel(ex4),ex4) == E5);
    ASSERT(intersectionRel(modk(100,4), modk(100,2)) == modk(100,4));
    ASSERT(intersectionRel(inverseRel(modk(100,2)), modk(100,2)) == emptyRelation(100));
    ASSERT(intersectionRel(modk(100,3), modk(100,2)) == modk(100,6));
    cout << "Fin des tests : intersection" << endl;

}

/*************************************************/
/**** Exercice 4 : Composition, transitivité  ****/
/*************************************************/

/** Retourne la composition de 2 relations
 * @param R1, une relation de taille n
 * @param R2, une relation de taille n,
 * @return la relation composition de R1 et R2
 * (on suppose que R1 et R2 ont la même taille)
 **/
Relation compositionRel(const Relation &R1, const Relation &R2) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction compositionRel
 **/
void testCompositionRel() {
    cout << "Début des tests : composition" << endl;
    Relation E5 = emptyRelation(5);
    Relation F5 = fullRelation(5);
    Relation D5 = diagonalRelation(5);
    ASSERT(compositionRel(E5,E5) == E5);
    ASSERT(compositionRel(F5,F5) == F5);
    ASSERT(compositionRel(E5,F5) == E5);
    ASSERT(compositionRel(F5,E5) == E5);
    ASSERT(compositionRel(D5,F5) == F5);
    ASSERT(compositionRel(D5,D5) == D5);
    Relation E0 = emptyRelation(0);
    Relation F0 = fullRelation(0);
    ASSERT(compositionRel(E0,E0) == E0);
    ASSERT(compositionRel(F0,F0) == F0);
    ASSERT(compositionRel(ex2,ex2) == ex2);
    ASSERT(compositionRel(D5,ex2) == ex2);
    ASSERT(compositionRel(F5,ex2) == F5);
    ASSERT(compositionRel(ex2,F5) == F5);
    ASSERT(compositionRel(ex3,ex3) == ex3);
    ASSERT(compositionRel(ex4,ex4) == ex4);
    ASSERT(compositionRel(ex4,ex5) == ex10);
    Relation R = compositionRel(ex5,ex5);
    ASSERT(R[0][2]);
    ASSERT(R[2][4]);
    ASSERT(not R[0][0]);
    ASSERT(not R[1][4]);
    R = modk(100,4);
    ASSERT(compositionRel(R,R) == R);
    cout << "Fin des tests : composition" << endl;

}

/** Teste de la transitivité
 * une relation R est transitive si R(i,j) and R(j,k) => R(i,k)
 * autrement dit, R est transitive ssi la composition de R par R est incluse dans R
 * @param R, une relation
 * @return true si la relation est transitive, false sinon
 **/
bool estTransitive(Relation R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estTransitive
 */
void testEstTransitive() {
    cout << "Début des tests : transitive" << endl;
    ASSERT(estTransitive(fullRelation(3)));
    ASSERT(estTransitive(emptyRelation(3)));
    ASSERT(estTransitive(ex1));
    ASSERT(estTransitive(ex2));
    ASSERT(estTransitive(ex3));
    ASSERT(estTransitive(ex4));
    ASSERT(estTransitive(ex6));
    ASSERT(not estTransitive(ex5));
    ASSERT(not estTransitive(ex7));
    ASSERT(estTransitive(modk(100,5)));
    ASSERT(estTransitive(modk(100,3)));
    ASSERT(estTransitive(plusPetitEgal(100)));
    ASSERT(estTransitive(diviseur(100)));
    ASSERT(estTransitive(exemplek(100,4)));
    ASSERT(not estTransitive(unionRel(modk(100,5),modk(100,3))));
    ASSERT(not estTransitive(unionRel(exemplek(100,3),exemplek(100,5))));
    cout << "Fin des tests : transitive" << endl;
}
/** Fonction d'illustration
 * L'union de 2 relations transitives n'est pas toujours transitive
 **/
void illustrationUnionTransitive() {
    cout << "L'union de deux relations transitives n'est pas toujours transitive, par exemple..." << endl;
}

/** Teste si R est une relation d'equivalence
 * (reflexive, transitive, symetrique)
 * @param R, une relation
 * @return true si R est une relation d'equivalence, false sinon
 **/
bool estEquivalence(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estEquivalence
 */
void testEstEquivalence() {
    cout << "Début des tests : equivalence" << endl;
    ASSERT(estEquivalence(fullRelation(3)));
    ASSERT(not estEquivalence(emptyRelation(3)));
    ASSERT(estEquivalence(ex1));
    ASSERT(not estEquivalence(ex2));
    ASSERT(not estEquivalence(ex3));
    ASSERT(estEquivalence(ex4));
    ASSERT(not estEquivalence(ex6));
    ASSERT(not estEquivalence(ex5));
    ASSERT(not estEquivalence(ex7));
    ASSERT(estEquivalence(modk(100,5)));
    ASSERT(estEquivalence(modk(100,3)));
    ASSERT(not estEquivalence(plusPetitEgal(100)));
    ASSERT(not estEquivalence(diviseur(100)));
    ASSERT(not estEquivalence(exemplek(100,4)));
    ASSERT(not estEquivalence(unionRel(modk(100,5),modk(100,3))));
    cout << "Fin des tests : equivalence" << endl;
}

/** Teste si R est une relation d'ordre
 * (reflexive, transitive, anti-symetrique)
 * @param R, une relation
 * @return true si R est une relation d'ordre, false sinon
 **/
bool estOrdre(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction estOrdre
 */
void testEstOrdre() {
    cout << "Début des tests : ordre" << endl;
    ASSERT(not estOrdre(fullRelation(3)));
    ASSERT(not estOrdre(emptyRelation(3)));
    ASSERT(estOrdre(ex1));
    ASSERT(estOrdre(ex2));
    ASSERT(not estOrdre(ex3));
    ASSERT(not estOrdre(ex4));
    ASSERT(estOrdre(ex6));
    ASSERT(not estOrdre(ex5));
    ASSERT(not estOrdre(ex7));
    ASSERT(not estOrdre(modk(100,5)));
    ASSERT(not estOrdre(modk(100,3)));
    ASSERT(estOrdre(plusPetitEgal(100)));
    ASSERT(not estOrdre(diviseur(100)));
    ASSERT(estOrdre(exemplek(100,4)));
    ASSERT(not estOrdre(unionRel(exemplek(100,5), exemplek(100,3))));
    cout << "Fin des tests : ordre" << endl;
}

/********************************/
/**** Exercice 5 : clotures  ****/
/********************************/

/** Retourne la cloture reflexive d'une relation
 * @param R, une relation
 * @return la relation obtenue par cloture reflexive de R
 **/
Relation clotureReflexive(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction clotureTransitive
 */
void testClotureReflexive() {
    cout << "Début des tests : cloture reflexive" << endl;
    Relation F3 = fullRelation(3);
    Relation E3 = emptyRelation(3);
    Relation D3 = diagonalRelation(3);
    ASSERT(clotureReflexive(F3) == F3);
    ASSERT(clotureReflexive(E3) == D3);
    ASSERT(clotureReflexive(ex1) == ex1);
    ASSERT(clotureReflexive(ex2) == ex2);
    ASSERT(clotureReflexive(ex4) == ex4);
    ASSERT(clotureReflexive(ex6) == ex6);
    Relation C = clotureReflexive(ex3);
    ASSERT(C != ex3);
    ASSERT(incluse(ex3,C));
    ASSERT(estReflexive(C));
    C = clotureReflexive(ex5);
    ASSERT(C != ex5);
    ASSERT(incluse(ex5,C));
    ASSERT(estReflexive(C));
    C = clotureReflexive(ex7);
    ASSERT(C != ex7);
    ASSERT(incluse(ex7,C));
    ASSERT(estReflexive(C));
    Relation R = modk(100,5);
    ASSERT(R == clotureReflexive(R));
    R = modk(100,3);
    ASSERT(R == clotureReflexive(R));
    R = plusPetitEgal(100);
    ASSERT(R == clotureReflexive(R));
    R = exemplek(100,4);
    ASSERT(R == clotureReflexive(R));
    R = diviseur(100);
    C = clotureReflexive(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estReflexive(C));
    cout << "Fin des tests : cloture reflexive" << endl;
}


/** Retourne la cloture symétrique d'une relation
 * @param R, une relation
 * @return la relation obtenue par cloture symétrique de R
 **/
Relation clotureSymetrique(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}


/** Test de la fonction clotureSymetrique
 */
void testClotureSymetrique() {
    cout << "Début des tests : cloture symetrique" << endl;
    Relation F3 = fullRelation(3);
    Relation E3 = emptyRelation(3);
    Relation D3 = diagonalRelation(3);
    ASSERT(clotureSymetrique(F3) == F3);
    ASSERT(clotureSymetrique(E3) == E3);
    ASSERT(clotureSymetrique(D3) == D3);
    ASSERT(clotureSymetrique(ex4) == ex4);
    Relation F5 = fullRelation(5);
    ASSERT(clotureSymetrique(ex2) == F5);
    ASSERT(clotureSymetrique(ex6) == F5);
    Relation C = clotureSymetrique(ex3);
    ASSERT(C != ex3);
    ASSERT(incluse(ex3,C));
    ASSERT(estSymetrique(C));
    C = clotureSymetrique(ex5);
    ASSERT(C != ex5);
    ASSERT(incluse(ex5,C));
    ASSERT(estSymetrique(C));
    C = clotureSymetrique(ex7);
    ASSERT(C != ex7);
    ASSERT(incluse(ex7,C));
    ASSERT(estSymetrique(C));
    Relation R = modk(100,5);
    ASSERT(R == clotureSymetrique(R));
    R = modk(100,3);
    ASSERT(R == clotureSymetrique(R));
    R = plusPetitEgal(100);
    ASSERT(clotureSymetrique(R) == fullRelation(100));
    R = diviseur(100);
    C = clotureSymetrique(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estSymetrique(C));
    R = exemplek(100,4);
    C = clotureSymetrique(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estSymetrique(C));
    cout << "Fin des tests : cloture symetrique" << endl;
}

/** Retourne la cloture transitive d'une relation
 * @param R, une relation
 * @return la relation obtenue par cloture transitive de R
 **/
Relation clotureTransitive(const Relation &R) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction clotureTransitive
 */
void testClotureTransitive() {
    cout << "Début des tests : cloture transitive" << endl;
    Relation F3 = fullRelation(3);
    Relation E3 = emptyRelation(3);
    ASSERT(clotureTransitive(F3) == F3);
    ASSERT(clotureTransitive(E3) == E3);
    ASSERT(clotureTransitive(ex1) == ex1);
    ASSERT(clotureTransitive(ex2) == ex2);
    ASSERT(clotureTransitive(ex3) == ex3);
    ASSERT(clotureTransitive(ex4) == ex4);
    ASSERT(clotureTransitive(ex6) == ex6);
    Relation C = clotureTransitive(ex5);
    ASSERT(C != ex5);
    ASSERT(incluse(ex5,C));
    ASSERT(estTransitive(C));
    C = clotureTransitive(ex7);
    ASSERT(C != ex7);
    ASSERT(incluse(ex7,C));
    ASSERT(estTransitive(C));
    Relation R = modk(100,5);
    ASSERT(R == clotureTransitive(R));
    R = modk(100,3);
    ASSERT(R == clotureTransitive(R));
    R = plusPetitEgal(100);
    ASSERT(R == clotureTransitive(R));
    R = diviseur(100);
    ASSERT(R == clotureTransitive(R));
    R = unionRel(modk(100,5),modk(100,3));
    C = clotureTransitive(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estTransitive(C));
    R = unionRel(exemplek(100,5),exemplek(100,3));
    C = clotureTransitive(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estTransitive(C));
    cout << "Fin des tests : cloture transitive" << endl;
}


/** Retourne la cloture transitive reflexive
 * @param R, une relation
 * @return la relation obtenue par cloture transitive et reflexive
 **/
Relation clotureTransRef(const Relation &R){
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction clotureTransRef
 */
void testClotureTransRef() {
    cout << "Début des tests : cloture transituve reflexive" << endl;
    Relation F3 = fullRelation(3);
    Relation E3 = emptyRelation(3);
    Relation D3 = diagonalRelation(3);
    ASSERT(clotureTransRef(F3) == F3);
    ASSERT(clotureTransRef(E3) == D3);
    ASSERT(clotureTransRef(D3) == D3);
    ASSERT(clotureTransRef(ex2) == ex2);
    ASSERT(clotureTransRef(ex4) == ex4);
    Relation C = clotureTransRef(ex3);
    ASSERT(C != ex3);
    ASSERT(incluse(ex3,C));
    ASSERT(estOrdre(C));
    C = clotureTransRef(ex5);
    ASSERT(C == ex2);
    C = clotureTransRef(ex7);
    ASSERT(C != ex7);
    ASSERT(incluse(ex7,C));
    ASSERT(estTransitive(C));
    ASSERT(estReflexive(C));
    Relation R = modk(100,5);
    ASSERT(R == clotureTransRef(R));
    R = modk(100,3);
    ASSERT(R == clotureTransRef(R));
    R = plusPetitEgal(100);
    ASSERT(clotureTransRef(R) == R);
    R = diviseur(100);
    C = clotureTransRef(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estOrdre(C));
    R = exemplek(100,4);
    ASSERT(R == clotureTransRef(R));
    R = unionRel(exemplek(100,3),exemplek(100,4));
    C = clotureTransRef(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estOrdre(C));
    cout << "Fin des tests : cloture transitive reflexive" << endl;
}

/** Retourne la cloture transitive reflexive et symétrique
 * @param R, une relation
 * @return la relation obtenue par cloture transitive, reflexive et symétrique
 **/
Relation clotureEquivalence(const Relation &R){
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction clotureEquivalence
 */
void testClotureEquivalence() {
    cout << "Début des tests : cloture equivalence" << endl;
    Relation F3 = fullRelation(3);
    Relation E3 = emptyRelation(3);
    Relation D3 = diagonalRelation(3);
    ASSERT(clotureEquivalence(F3) == F3);
    ASSERT(clotureEquivalence(E3) == D3);
    ASSERT(clotureEquivalence(D3) == D3);
    ASSERT(clotureEquivalence(ex4) == ex4);
    Relation F5 = fullRelation(5);
    ASSERT(clotureEquivalence(ex2) == F5);
    ASSERT(clotureEquivalence(ex6) == F5);
    Relation C = clotureEquivalence(ex3);
    ASSERT(C != ex3);
    ASSERT(incluse(ex3,C));
    ASSERT(estEquivalence(C));
    C = clotureEquivalence(ex5);
    ASSERT(C != ex5);
    ASSERT(incluse(ex5,C));
    ASSERT(estEquivalence(C));
    C = clotureEquivalence(ex7);
    ASSERT(C != ex7);
    ASSERT(incluse(ex7,C));
    ASSERT(estEquivalence(C));
    Relation R = modk(100,5);
    ASSERT(R == clotureEquivalence(R));
    R = modk(100,3);
    ASSERT(R == clotureEquivalence(R));
    R = plusPetitEgal(100);
    ASSERT(clotureEquivalence(R) == fullRelation(100));
    R = diviseur(100);
    C = clotureEquivalence(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estEquivalence(C));
    R = exemplek(100,4);
    C = clotureEquivalence(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estEquivalence(C));
    R = unionRel(modk(100,3),modk(100,4));
    C = clotureEquivalence(R);
    ASSERT(R != C);
    ASSERT(incluse(R,C));
    ASSERT(estEquivalence(C));
    cout << "Fin des tests : cloture equivalence" << endl;
}

/*****************************************************************/

void allTests(){
    // testAfficheMatriceRelation();
    // testAfficheCouplesRelation();
    // testPlusPetitEgal();
    // testDiviseur();
    // testModK();
    // testIncluse();
    // testEstReflexive();
    // testUnionRel();
    // testIntersectionRel();
    // testInverseRel();
    // testCompositionRel();
    // testEstSymetrique();
    // testEstAntiSymetrique();
    // testEstTransitive();
    // testClotureTransitive();
    // testClotureReflexive();
    // testClotureSymetrique();
    // testEstEquivalence();
    // testEstOrdre();
    // testClotureEquivalence();
    // testClotureTransRef();
}

void allIllustrations() {
    // illustrationReflexiveSymetrique();
    // illustrationSymetriqueAntisymetrique();

}

int main() {
    allTests();
    allIllustrations();
}



