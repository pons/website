#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
using namespace std;

/** Infrastructure minimale de test **/
#define ASSERT(test) if (!(test)) cout << "Test failed in file " << __FILE__ << " line " << __LINE__ << ": " #test << endl;

/******** Fonctions utiles pour les tests ********/

double evalProba(int (*alea)(), int val, int n) {
    double c = 0;
    for(int i=0; i<n; i++) {
        if(alea() == val) c++;
    }
    return c/n;
}

bool closeEnough(double a, double b) {
    double d = b-a;
    return (d <= .03 and d >= -.03);
}

bool testProba(int (*alea)(), int val, double proba) {
    return closeEnough(evalProba(alea,val,10000),proba);
}

/*************************************************/


/** Retourne un entier aléatoire entre a et b (inclus) selon
 * une distribution uniforme
 * @param a, la borne inférieure
 * @param b, la borne supérieure
 * @return un entier a <= n <= b
 **/
int randint (int a, int b) {
    return rand()%(b+1-a)+a;
}

/** Retourne un double pris uniformément sur [0,1]
 **/
double rand01() {
    return rand()/(double)RAND_MAX;
}

/** Affiche un tableau d'entier sous forme de diagramme en colonnes
 * @param t un tableau d'entier
 **/
void afficheDistribution(vector<int> t) {
    int s = 0;
    for(unsigned int i = 0; i < t.size(); i++) s+=t[i];
    double r = 20./s;
    for(int j =0; j < 20; j++) {
        for(unsigned i =0; i< t.size(); i++) {
            if (r*t[i] >= 20-j) {
                cout << " ___ " ;
            } else {
                cout << "     " ;
            }

        }
        cout << endl;
    }
    cout << endl;
    for (unsigned int i =0 ; i < t.size(); i++) {
        cout << t[i] << " ";
    }
    cout << endl;
}

/******************************************/
/**** Exercice 1 : Jet de dès          ****/
/******************************************/

/** Resultat d'un tirage de dé régulier à 6 faces
 * (distri uniforme)
 * @return le resultat du dé
 **/
int deUniforme() {
    return randint(1,6);
}

/** Exemple d'utilisation de deUniforme
 **/
void exempleDeUniforme() {
    cout << "Début exemple deUniforme" << endl;
    for(int i = 0; i < 10; i++) {
        cout << "Je tire un dé et je vois : " << deUniforme() <<endl;
    }
    cout << "Fin exemple deUniforme" << endl;
}

/** Resultat d'un tirage d'un dé à 4 faces non régulier
 * @return un nombre entre 1 et 4 (le résultat du dé)
 **/
int de4Pipe() {
    int tirage = randint(1,5);
    if(tirage <= 3) {
        return tirage;
    } else {
        return 4;
    }
}

/** Exemple de la fonction de4Pipe
 **/
void exempleDe4Pipe() {
    cout << "Début exemple de4Pipe" << endl;
    for(int i = 0; i < 10; i++) {
        cout << "Je tire un dé et je vois : " << de4Pipe() <<endl;
    }
    cout << "Probabilité d'obtenir 1 : ";
    cout << evalProba(de4Pipe, 1, 10000) << endl;
    cout << "Probabilité d'obtenir 2 : ";
    cout << evalProba(de4Pipe, 2, 10000) << endl;
    cout << "Probabilité d'obtenir 3 : ";
    cout << evalProba(de4Pipe, 3, 10000) << endl;
    cout << "Probabilité d'obtenir 4 : ";
    cout << evalProba(de4Pipe, 4, 10000) << endl;
    cout << "Fin exemple de4Pipe" << endl;
}

/** Resultat d'un tirage de dé pipé à 6 faces
 * p(1) = p(2) = p(3) = p(4) = p(5) = 1/8
 * p(6) = 3/8
 * @return un nombre entre 1 et 6 : le resultat du dé
 **/
int dePipe() {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction dePipe
 **/
void testDePipe() {
    cout << "Début des tests : de pipe" << endl;
    ASSERT(testProba(dePipe,1,1./8.));
    ASSERT(testProba(dePipe,2,1./8.));
    ASSERT(testProba(dePipe,3,1./8.));
    ASSERT(testProba(dePipe,4,1./8.));
    ASSERT(testProba(dePipe,5,1./8.));
    ASSERT(testProba(dePipe,6,3./8.));
    cout << "Fin des tests : de pipe" << endl;
}


/** Distrion des lancers d'un dé uniforme
 * @param N le nombre de lancés
 * @return un tableau tel que la case i contienne le nombre de valeurs (i+1)
 **/
vector<int> distriDeUniforme(int N) {
    vector<int> distribution = vector<int>(6,0); // Crée un tableau de taille 6 initialisé à 0
    // Effectuer N tirages pour évaluer la distribution

    return distribution;
}

/** Test de la fonction distri de uniforme
 **/
void testDistriDeUniforme() {
    cout << "Début des tests : distribution dé uniforme" << endl;
    vector<int> d = distriDeUniforme(10000);
    for(int i =0; i < 6; i++) {
        ASSERT(closeEnough(d[i]/10000., 1./6.));
    }
    afficheDistribution(d);
    cout << "Fin des tests : distribution dé uniforme" << endl;
}

/** Distrion des lancers d'un dé pipé
 * @param N le nombre de lancés
 * @return un tableau tel que la case i contienne le nombre de valeurs (i+1)
 **/
vector<int> distriDePipe(int N) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction distri de pipé
 **/
void testDistriDePipe() {
    cout << "Début des tests : distribution dé pipé" << endl;
    vector<int> d = distriDePipe(10000);
    for(int i =0; i < 5; i++) {
        ASSERT(closeEnough(d[i]/10000., 1./8.));
    }
    ASSERT(closeEnough(d[5]/10000., 3./8.));
    afficheDistribution(d);
    cout << "Fin des tests : distribution dé pipé" << endl;
}

/** Distrion de la somme de dés (uniforme)
 * @param N le nombre de lancés
 * @return un tableau de taille 12 tel que la case i contienne le nombre de valeurs (i+1)
 **/
vector<int> distriSommeDes(int N) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction distri somme dés
 **/
void testDistriSommeDes() {
    cout << "Début des tests : distribution somme dés" << endl;
    vector<int> d = distriSommeDes(10000);
    ASSERT(d[0] == 0);
    ASSERT(closeEnough(d[1]/10000., 1./36.));
    ASSERT(closeEnough(d[2]/10000., 2./36.));
    ASSERT(closeEnough(d[3]/10000., 3./36.));
    ASSERT(closeEnough(d[4]/10000., 4./36.));
    ASSERT(closeEnough(d[5]/10000., 5./36.));
    ASSERT(closeEnough(d[6]/10000., 6./36.));
    ASSERT(closeEnough(d[7]/10000., 5./36.));
    ASSERT(closeEnough(d[8]/10000., 4./36.));
    ASSERT(closeEnough(d[9]/10000., 3./36.));
    ASSERT(closeEnough(d[10]/10000., 2./36.));
    ASSERT(closeEnough(d[11]/10000., 1./36.));
    afficheDistribution(d);
    cout << "Fin des tests : distribution somme dés" << endl;
}

/**************************************/
/**** Exercice 2 : Pieces          ****/
/**************************************/

/** Tirage pile / face sur une piece
 * @param p, la probablité d'obtenir pile
 * @return true si la pièce est pile, false si face
 **/
bool piece(double p) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction piece
 **/
int piece1() { return piece(0.5); }
int piece2() { return piece(0.25); }
void testPiece() {
    cout << "Début des tests : piece" << endl;
    ASSERT(testProba(piece1, 1, 0.5));
    ASSERT(testProba(piece2, 1, 0.25));
    cout << "Fin des tests : piece" << endl;
}

/** Simulation d'une expérience de n lancés de pièce avec comptage des piles
 * @param n, le nombre de lancés
 * @param p, la probabilité que la pièce à de tomber sur pile
 * @return le nombre 0 <= k <= n de fois où la pièce est retombée sur pile
 **/
int nbPiles(int n, double p) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction nbPiles
 * (ces tests sont très limités, on testera la loi de proba plus tard !)
 **/
void testNbPiles() {
    cout << "Début des tests : nb piles" << endl;
    ASSERT(nbPiles(10,0.5) >=0);
    ASSERT(nbPiles(10,0.5) <=10);
    ASSERT(nbPiles(10,0) ==0);
    ASSERT(nbPiles(10,1) ==10);
    cout << "Fin des tests : nb piles" << endl;
}

/** Evaluation expérimentale de la probabilité pnk
 * @param N, le nombre de fois où l'on va répéter l'expérience
 * @param n, le nombre de lancés pour chaque expérience
 * @param k, le nombre exact de piles que l'on souhaite obtenir
 * @param p, la probabilité utilisée pour lancer la pièce
 * @return la proportion d'expérience qui ont donné exactement k piles
 **/
double pnkEval(int N, int n, int k, double p) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne

    /* ATTENTION : le résultat doit être un double, la division de 2 entiers en C++ est toujours un entier
     * Pour faire une "vraie" division, il faut qu'au moins un des deux nombres soit un double
     */
}

/** Test de la fonction PnkEval
 * (ces tests sont très limités, on testera la loi de proba plus tard !)
 **/
void testPnkEval() {
    cout << "Début des tests : pnkEval" << endl;
    ASSERT(pnkEval(100,10,5,0.5) > 0); /* Si vous avez faux ici, c'est sans doute que vous faites une division entière */
    ASSERT(pnkEval(100,10,5,0.5) < 10);
    ASSERT(pnkEval(100,10,5,0) == 0);
    ASSERT(pnkEval(100,10,0,0) == 1);
    ASSERT(pnkEval(100,10,10, 1) ==1);
    cout << "Fin des tests : pnkEval" << endl;
}

/** Calcul Récursif de la probablité pnk
 * @param n, le nombre de lancés
 * @param k, le nombre recherché de piles
 * @param p la probablité d'obtenir pile
 * @return la probablité d'obtenir exactement k piles en n lancés
 **/
double pnkRec(int n, int k, double p) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction pnkRec
 **/
void testPnkRec() {
    cout << "Début des tests : pnkRec" << endl;
    ASSERT(pnkRec(1,1,0.25) == 0.25);
    ASSERT(pnkRec(1,0,0.25) == 0.75);
    ASSERT(pnkRec(1,2,0.25) == 0);
    ASSERT(pnkRec(2,1,0.25) == 2*0.25*0.75);
    ASSERT(pnkRec(2,2,0.5) == 0.25);
    ASSERT(pnkRec(2,0,0.5) == 0.25);
    ASSERT(pnkRec(0,0,0.5) == 1);
    cout << "Test avec l'évaluation" << endl;
    double peval = pnkEval(100,5,3,0.45);
    double preel = pnkRec(5,3,0.45);
    cout << "eval (100 essais) " << peval << endl;
    peval = pnkEval(1000,5,3,0.45);
    cout << "eval (1000 essais) " << peval << endl;
    peval = pnkEval(10000,5,3,0.45);
    cout << "eval (10000 essais) " << peval << endl;
    cout << "reel : " << preel << endl;
    ASSERT(closeEnough(peval, preel));
    cout << "Fin des tests : pnkRec" << endl;
}


/** Calcul de la probablité pnk avec un double tableau
 * @param n, le nombre de lancés
 * @param k, le nombre recherché de piles
 * @param p la probablité d'obtenir pile
 * @return la probablité d'obtenir exactement k piles en n lancés
 **/
double pnkDyn(int n, int k, double p) {
    /* cas particulier */

    /* Allocation et initialisation à 0 d'un tableau à n+1 lignes et k+1 colonnes */
    vector<vector<double>> t = vector<vector<double>>(n+1,vector<double>(k+1,0));
    t[0][0] = 1; /* proba d'obtenir exactement 0 pile avec 0 lancés */
    /* faire en sorte que t[i][j] soit la proba d'obtenir exactement j fois piles après i lancés */

    return t[n][k];
}

/** Test de la fonction pnkDyn
 **/
void testPnkRDyn() {
    cout << "Début des tests : pnkDyn" << endl;
    ASSERT(pnkDyn(1,1,0.25) == 0.25);
    ASSERT(pnkDyn(1,0,0.25) == 0.75);
    ASSERT(pnkDyn(1,2,0.25) == 0);
    ASSERT(pnkDyn(2,1,0.25) == 2*0.25*0.75);
    ASSERT(pnkDyn(2,2,0.5) == 0.25);
    ASSERT(pnkDyn(2,0,0.5) == 0.25);
    ASSERT(pnkDyn(0,0,0.5) == 1);
    cout << "Test avec l'évaluation" << endl;
    double peval = pnkEval(100,50,30,0.45);
    double preel = pnkDyn(50,30,0.45);
    cout << "eval (100 essais) " << peval << endl;
    peval = pnkEval(1000,50,30,0.45);
    cout << "eval (1000 essais) " << peval << endl;
    peval = pnkEval(10000,50,30,0.45);
    cout << "eval (10000 essais) " << peval << endl;
    peval = pnkEval(100000,50,30,0.45);
    cout << "eval (100000 essais) " << peval << endl;
    cout << "reel : " << preel << endl;
    ASSERT(closeEnough(peval, preel));
    cout << "Fin des tests : pnkRec" << endl;
}

/*****************************************/
/**** Exercice 3 : Le choix gagnant   ****/
/*****************************************/

int boiteGagnante() {
    return randint(1,3);
}

/** Retourne la boite dévoilée par le maitre du jeu prise au hasard
 * connaissant la boite gagnante et le choix du joueur
 * @param gagnante, le numéro de la boite gagnante
 * @param choixJoueur, le numéro choisi par le joueur
 * @return le numéro d'une boite perdante au hasard qui n'est pas celle du joueur
 ***/
int boiteDevoilee(int gagnante, int choixJoueur) {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction boiteDevoilee
 **/
int boiteDevoilee11() { return boiteDevoilee(1,1);}
void testBoiteDevoilee() {
    cout << "Début des tests : boite devoilee" << endl;
    ASSERT(boiteDevoilee(1,2) == 3);
    ASSERT(boiteDevoilee(1,3) == 2);
    ASSERT(boiteDevoilee(2,1) == 3);
    ASSERT(boiteDevoilee(2,3) == 1);
    ASSERT(boiteDevoilee(3,1) == 2);
    ASSERT(boiteDevoilee(3,2) == 1);
    ASSERT(boiteDevoilee(1,1) != 1);
    ASSERT(boiteDevoilee(2,2) != 2);
    ASSERT(boiteDevoilee(3,3) != 3);
    ASSERT(testProba(boiteDevoilee11,2,0.5));
    cout << "Fin des tests : boite devoilee" << endl;

}

/** Simulation d'une partie où le joueur ne change pas d'avis
 * @return 1 si le joueur gagne et 0 sinon
 **/
int joueurMemeAvis() {
    int b = boiteGagnante();
    int choixJoueur = randint(1,3);
    return (b == choixJoueur);
}

void testJoueurMemeAvis() {
    cout << "Début des tests : joueur meme avis" << endl;
    ASSERT(testProba(joueurMemeAvis,1,1./3.));
    cout << "Fin des tests : joueur meme avis" << endl;
}

/** Simulation d'une partie où le joueur change d'avis après que le maître
 * de jeu ait dévoilé une boite vide
 * @return 1 si le joueur gagne et 0 sinon
 **/
int joueurChangeAvis() {
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

int main() {
    /** La ligne suivante sert à initialier le tirage aléatoire (ne la commentez pas) **/
    srand(time(NULL));
    /******************************************/
    // exempleDeUniforme();
    // exempleDe4Pipe();
    // testDePipe();
    // testDistriDeUniforme();
    // testDistriDePipe();
    // testDistriSommeDes();
    // testPiece();
    // testNbPiles();
    // testPnkEval();
    // testPnkRec();
    // testPnkRDyn();
    // testBoiteDevoilee();
    //testJoueurMemeAvis();
    // cout << evalProba(joueurChangeAvis,1,10000) << endl;
}
