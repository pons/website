#include <vector>
#include <iostream>
using namespace std;

/** Infrastructure minimale de test **/
#define ASSERT(test) if (!(test)) cout << "Test failed in file " << __FILE__ << " line " << __LINE__ << ": " #test << endl;


/*************************************/
/**** Exercice 1 : Mise en jambe  ****/
/*************************************/

/** REMARQUE
 * Pour éviter la recopie de tableaux,
 * TOUS LES PARAMETRES SONT PASSES PAR REF (avec le symbole &)
 * on utilise "const" quand le tableau ne sera PAS modifié par la fonction
 * (révision du L1 !)
 **/


/** Affiche le contenu d'un ensemble
 * @param ens un ensemble représenté par un vector d'entier
 **/
void afficheEnsemble(const vector<int> &ens) {
   // Ecrire le code ici
   // la ligne suivante n'est là que parce que la fonction est vide
    // SUPPRIMEZ LA quand vous avez écrit votre code
   cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction afficheEnsemble d'un ensemble
 **/
void testAffiche(){
    afficheEnsemble({1,5,2,6,45});
    afficheEnsemble({});
    afficheEnsemble({3});
}

/** Affiche toutes les représentation de {1,2,3}
 **/
void afficheEnsemble123(){
    // Exemple :
    afficheEnsemble({1,2,3});
    // Ecrivez les 5 autres possibilités
}

/***************************************/
/**** Exercice 2 : Ensembles égaux  ****/
/***************************************/

/** Teste l'appartenance d'un entier à un ensemble donné
 * @param a, un entier
 * @param ens, un ensemble d'entier
 * @return true si a appartient à ens, false sinon
 **/
bool appartient(int a, const vector<int> &ens) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction appartient
 **/
void testAppartient() {
    cout << "Début des tests : appartient" << endl;
    ASSERT(appartient(1,{1,2,3}));
    ASSERT(appartient(2,{1,2,3}));
    ASSERT(appartient(3,{1,2,3}));
    ASSERT(not appartient(5,{1,2,3}));
    ASSERT(appartient(8,{1,5,22,8,3,45}));
    ASSERT(not appartient(4,{}));
    cout << "Fin des tests : appartient" << endl;
}

/** Teste l'inclusion d'un ensemble dans un autre
 * @param ens1, un premier ensemble
 * @param ens2, un deuxième ensemble
 * @return true si ens1 est inclus dans ens2, false sinon
 **/
bool inclus(const vector<int> &ens1, const vector<int> &ens2) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction inclus
 **/
void testInclus() {
    cout << "Début des tests : inclus" << endl;
    ASSERT(inclus({1},{1,2,3}));
    ASSERT(inclus({2},{1,2,3}));
    ASSERT(inclus({3},{1,2,3}));
    ASSERT(inclus({1,2,3},{1,2,3}));
    ASSERT(inclus({2,1,3},{1,2,3}));
    ASSERT(inclus({1,2,3},{3,1,2}));
    ASSERT(inclus({2,4,1},{3,4,2,5,9,1}));
    ASSERT(inclus({},{1,2,3}));
    ASSERT(not inclus({4},{1,2,3}));
    ASSERT(not inclus({3,4,2,5,9,1},{1,2,3}));
    ASSERT(not inclus({1,2,3,4},{1,2,3}));
    ASSERT(not inclus({2,4,8,1},{3,4,2,5,9,1}));
    ASSERT(not inclus({2,4,1,8},{3,4,2,5,9,1}));
    ASSERT(not inclus({1,2,3},{}));
    cout << "Fin des tests : inclus" << endl;

}

/** Teste l'egalité de deux ensemble par la double inclusion
 * @param ens1, un premier ensemble
 * @param ens2, un deuxième ensemble
 * @return true si les deux ensembles sont egauc et false sinon
 **/
bool egal(const vector<int> &ens1, const vector<int> &ens2) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction egal
 **/
void testEgal() {
    cout << "Début des tests : egal" << endl;
    ASSERT(egal({1,2,3},{1,2,3}));
    ASSERT(egal({1,2,3},{1,3,2}));
    ASSERT(egal({2,1,3},{1,2,3}));
    ASSERT(egal({2,3,1},{1,2,3}));
    ASSERT(egal({1,2,3},{3,1,2}));
    ASSERT(egal({1,2,3},{3,2,1}));
    ASSERT(egal({1},{1}));
    ASSERT(egal({},{}));
    ASSERT(egal({24,47,12,124,2},{47,2,124,24,12}));
    ASSERT(not egal({1,4,2,3},{1,2,3}));
    ASSERT(not egal({2},{3}));
    ASSERT(not egal({1,4,2},{1,2,3}));
    ASSERT(not egal({1,4,2},{1,3,4,2}));
    ASSERT(not egal({},{1,2}));
    cout << "Fin des tests : egal" << endl;
}

/***********************************************/
/**** Exercice 3 : Opérations élémentaires  ****/
/***********************************************/

/** Ajout dans un ensemble
 * Si `a` n'appartient pas à `ens`, la fonction l'ajoute,
 * sinon l'ensemble est laissé inchangé
 * @param a, un entier
 * @param ens, un ensemble
 **/
void ajoute(int a, vector<int> &ens) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction ajoute
 */
void testAjoute() {
    cout << "Début des tests : ajoute" << endl;
    vector<int> ens = vector<int>();
    ajoute(1,ens);
    ASSERT(egal(ens,{1}));
    ajoute(1,ens);
    ASSERT(egal(ens,{1}));
    ASSERT(ens.size() == 1);
    ajoute(2,ens);
    ASSERT(egal(ens,{1,2}));
    ajoute(2,ens);
    ASSERT(egal(ens,{1,2}));
    ASSERT(ens.size() == 2);
    ajoute(32,ens);
    ASSERT(egal(ens,{1,2,32}));
    ajoute(1,ens);
    ASSERT(egal(ens,{1,2,32}));
    ASSERT(ens.size() == 3);
    ajoute(2,ens);
    ASSERT(egal(ens,{1,2,32}));
    ASSERT(ens.size() == 3);
    ajoute(32,ens);
    ASSERT(egal(ens,{1,2,32}));
    ASSERT(ens.size() == 3);
    ajoute(4,ens);
    ASSERT(egal(ens,{1,2,4,32}));
    ASSERT(ens.size() == 4);
    cout << "Fin des tests : ajoute" << endl;
}

/** Union de deux ensembles
 * @param ens1, un premier ensemble
 * @param ens2, un deuxième ensemble
 * @return vector de int contenant l'ensemble union de `ens1` et `ens2`
 **/
vector<int> unionEns(const vector<int> &ens1, const vector<int> &ens2) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction unionEns
 */
void testUnionEns() {
    cout << "Début des tests : union" << endl;
    ASSERT(egal(unionEns({1,2,3},{1,2,3}),{1,2,3}));
    ASSERT(unionEns({1,2,3},{1,2,3}).size() == 3);
    ASSERT(egal(unionEns({2},{1,3}),{1,2,3}));
    ASSERT(egal(unionEns({1,2,56,3,4},{3,23,2,4,1}),{1,2,3,4,23,56}));
    ASSERT(unionEns({1,2,56,3,4},{3,23,2,4,1}).size() == 6);
    ASSERT(egal(unionEns({},{1,3,4,2}),{1,2,3,4}));
    ASSERT(egal(unionEns({},{}),{}));
    ASSERT(egal(unionEns({},{1}),{1}));
    ASSERT(egal(unionEns({2},{}),{2}));
    cout << "Fin des tests : union" << endl;
}


/** Intersection de deux ensembles
 * @param ens1, un premier ensemble
 * @param ens2, un deuxième ensemble
 * @return vector de int contenant l'ensemble intersection de `ens1` et `ens2`
 **/
vector<int> intersectionEns(const vector<int> &ens1, const vector<int> &ens2) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction intersectionEns
 */
void testIntersectionEns() {
    cout << "Début des tests : intersection" << endl;
    ASSERT(egal(intersectionEns({1,2,3},{1,2,3}),{1,2,3}));
    ASSERT(intersectionEns({1,2,3},{1,2,3}).size() == 3);
    ASSERT(egal(intersectionEns({2},{1,3}),{}));
    ASSERT(egal(intersectionEns({1,2,56,3,4},{3,23,2,4,1}),{1,2,3,4}));
    ASSERT(intersectionEns({1,2,56,3,4},{3,23,2,4,1}).size() == 4);
    ASSERT(egal(intersectionEns({},{1,3,4,2}),{}));
    ASSERT(egal(intersectionEns({},{}),{}));
    ASSERT(egal(intersectionEns({},{1}),{}));
    ASSERT(egal(intersectionEns({2},{}),{}));
    cout << "Fin des tests : intersection" << endl;
}

/** Soustraction de deux ensembles
 * @param ens1, un premier ensemble
 * @param ens2, un deuxième ensemble
 * @return vector de int contenant la soustraction de `ens1` par `ens2`
 **/
vector<int> soustractionEns(const vector<int> &ens1, const vector<int> &ens2) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction soustractionEns
 */
void testSoustractionEns() {
    cout << "Début des tests : soustraction" << endl;
    ASSERT(egal(soustractionEns({1,2,3},{1,2,3}),{}));
    ASSERT(egal(soustractionEns({1,2,3},{}),{1,2,3}));
    ASSERT(egal(soustractionEns({2},{1,3}),{2}));
    ASSERT(egal(soustractionEns({1,2,56,3,4},{3,23,2,4,1}),{56}));
    ASSERT(egal(soustractionEns({1,2,56,3,4},{3,23,1}),{2,4,56}));
    ASSERT(egal(soustractionEns({},{1,3,4,2}),{}));
    ASSERT(egal(soustractionEns({},{}),{}));
    ASSERT(egal(soustractionEns({},{1}),{}));
    ASSERT(egal(soustractionEns({2},{}),{2}));
    cout << "Fin des tests : soustraction" << endl;
}

/** Différence symétrique de deux ensembles
 * @param ens1, un premier ensemble
 * @param ens2, un deuxième ensemble
 * @return vector de int contenant la différence symétrique de `ens1` et `ens2`
 **/
vector<int> differenceEns(const vector<int> &ens1, const vector<int> &ens2) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction differenceEns
 */
void testDifferenceEns() {
    cout << "Début des tests : différence symétrique" << endl;
    ASSERT(egal(differenceEns({1,2,3},{1,2,3}),{}));
    ASSERT(egal(differenceEns({1,2,3},{}),{1,2,3}));
    ASSERT(egal(differenceEns({},{1,2,3}),{1,2,3}));
    ASSERT(egal(differenceEns({2},{1,3}),{1,2,3}));
    ASSERT(egal(differenceEns({1,2,56,3,4},{3,23,2,4,1}),{23,56}));
    ASSERT(egal(differenceEns({1,2,56,3,4},{3,23,1}),{2,4,23,56}));
    ASSERT(egal(differenceEns({},{1,3,4,2}),{1,2,3,4}));
    ASSERT(egal(differenceEns({},{}),{}));
    ASSERT(egal(differenceEns({},{1}),{1}));
    ASSERT(egal(differenceEns({2},{}),{2}));
    cout << "Fin des tests : différence symétrique" << endl;
}

/********************************************/
/**** Exercice 4 : Ensemble des parties  ****/
/********************************************/

/** Retourne un bit forme de booléen
 * @param n, un entier
 * @param pos, un entier
 * @return true, si le bit en position `pos` de `n` est 1, false sinon (bit de poids faible = position 0)
 **/
bool bitn(int n, int pos) {
    return (n >> pos)&1; // on décale vers la droite et on effectue un masque binaire avec 1
}

/** Crée le sous-ensemble correspondant à l'entier n
 * @param ens, un ensemble
 * @param n, un entier
 * @return le sous-ensemble de `ens` encodé par les bits de l'entier `n`
 **/
vector<int> sousEnsemble(const vector<int> &ens, int n) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction sousEnsemble
 **/
void testSousEnsemble() {
    cout << "Début des tests : sous ensemble" << endl;
    ASSERT(egal(sousEnsemble({1,2,3},0),{}));
    ASSERT(egal(sousEnsemble({1,2,3},1),{1}));
    ASSERT(egal(sousEnsemble({1,2,3},2),{2})); // binaire 10
    ASSERT(egal(sousEnsemble({1,2,3},3),{1,2})); // binaire 11
    ASSERT(egal(sousEnsemble({1,2,3},4),{3})); // binaire 100
    ASSERT(egal(sousEnsemble({1,2,3},5),{1,3})); // binaire 101
    ASSERT(egal(sousEnsemble({1,2,3},6),{2,3})); // binaire 110
    ASSERT(egal(sousEnsemble({1,2,3},7),{1,2,3})); // binaire 111
    cout << "Fin des tests : sous ensemble" << endl;
}

/** Retourne le nombre de sous ensemble (sans les fabriquer)
 * @param ens, un ensemble
 * @return le nombre de sous-ensembles de `ens`
 **/
int nbSousEnsembles(const vector<int> &ens) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

/** Test de la fonction nbSousEnsembles
 **/
void testNbSousEnsembles() {
    cout << "Début des tests : nombre de sous ensembles" << endl;
    ASSERT(nbSousEnsembles({}) == 1);
    ASSERT(nbSousEnsembles({1}) == 2);
    ASSERT(nbSousEnsembles({1,2}) == 4);
    ASSERT(nbSousEnsembles({4,2,6}) == 8);
    ASSERT(nbSousEnsembles({4,2,6,1,5,36,26}) == 128);
    cout << "Fin des tests : nombre de sous ensembles" << endl;
}

/** Affiche les sous ensembles d'un ensemble donné
 * @param ens, un ensemble d'entiers
 **/
void afficheSousEnsembles(const vector<int> &ens) {
    // Ecrire le code ici
    cerr << "Erreur : fonction non implantée" << endl; throw 0; // Supprimer cette ligne
}

int main(){

    // Décommenter les lignes au fur et à mesure de l'implantation
    // testAffiche();
    // afficheEnsemble123();
    // testAppartient();
    // testInclus();
    // testEgal();
    // testAjoute();
    // testUnionEns();
    // testIntersectionEns();
    // testSoustractionEns();
    // testDifferenceEns();
    // testSousEnsemble();
    // testNbSousEnsembles();
    // afficheSousEnsembles({1,2,3}); // doit afficher 8 sous-ensembles (dont le vide)
    // afficheSousEnsembles({5,6}); // doit afficher 4 sous-ensembles (dont le vide)

}
