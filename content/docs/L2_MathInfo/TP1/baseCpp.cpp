#include <vector> // pour utiliser les tableau
#include <iostream> // pour utiliser les entrées -- sorties
using namespace std; // bibliothèque standard

/** Infrastructure minimale de test **/
#define ASSERT(test) if (!(test)) cout << "Test failed in file " << __FILE__ << " line " << __LINE__ << ": " #test << endl;

/** La syntaxe du C++ est similaire à celle du C, ce fichier donne plusieurs exemples commentés **/

/** Une fonction C++ : typeDeRetour nomDeFonction(paramètres) { ... }
 * On documente toutes les fonction avec une syntaxe type javadoc
 * @param a, un entier
 * @param b, un entier
 * @return un entier (int)
 **/
int voiciMaFonction(int a, int b) {
    int reponse; // tout comme en C, il faut déclarer ses variables
    if (a > b) { // syntaxe d'un if, similaire au C
        reponse = a -b;
    } else {
        reponse = b - a;
    }
    return reponse; // si vous venez de python, n'oubliez pas de terminer toutes vos instructions par des ;
}

/** Fonction principale : celle qui sera appelée à l'exécution du programme
 **/
int main() {

    int a,b;

    ///////////////////////////////////////
    // Entrée -- sortie (différent du C !)
    ///////////////////////////////////////

    // Afficher un message
    cout << "Entrez deux nombres" << endl; // endl signifie retour à la ligne
    // Lecture de deux nombres entrés par l'utilisateur
    cin >> a; // remarquez que les chevrons ne sont pas dans le même sens que pour cout
    cin >> b;

    int r = voiciMaFonction(a,b);
    // on peut afficher plusieurs messages en les mettant à la suite, séparés par les chevrons
    cout << "La différence en valeur absolue entre " << a << " et " << b << " est " << r << endl;

    //////////////////////////////////////
    // Tableaux vector
    /////////////////////////////////////

    // Par rapport au C, le C++ offre la possibilité de travailler avec des tableaux plus riches : vector

    // Création d'un tableau vide de type entier

    vector<int> tab = vector<int>(); // Notez bien la syntaxe : chevrons, parenthèse etc

     // le tableau contient sa taille

     cout << "La taille de tab est " << tab.size() << endl;

     // on peut rajouter des éléments à la fin
     tab.push_back(5);

     cout << "A présent, la taille de tab est " << tab.size() << endl;
     cout << "Il contient : " << tab[0] << endl; // L'accès aux valeurs du tableau se fait comme en C

     // Création d'un tableau fixe avec des valeurs

     vector<int> tab2 = {4,8,2};
     cout << "La taille de tab2 est " << tab2.size() << endl;
     cout << "Il contient : " << tab2[0] << ", " << tab2[1] << " et " << tab2[2] << endl; // L'accès aux valeurs du tableau se fait comme en C

     // Boucle For
     for(int i = 0; i < tab2.size(); i++) {
        cout << tab2[i] << " ";
     }
     cout << endl;

     // boucle while
     int i = 0;
     while(i < tab2.size()) {
        cout << tab2[i] << " ";
        i++; // i = i+1
     }
     cout << endl;

    //////////////////////////////////////
    // Les Tests
    /////////////////////////////////////

    // Nous utilons une interface de test minimale personnalisée

    // Si le test passe, rien ne s'affiche
    ASSERT(8 > 5);

    // Si le test ne passe pas, le programme affiche une erreur à l'exécution
    ASSERT(5 > 8);

}




