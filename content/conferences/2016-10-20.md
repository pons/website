Title: Codima School
date: 2016-10-20 00:00
slug: 2016-10-20-conference
lang: en
location: Edinbourgh
conference_date_start: 2016-10-17
conference_date_end: 2016-10-21
conference_url: http://www.codima.ac.uk/school2016/
slides: 2016_Codima.pdf
school: true
tags: Sage, Research School
role: Sage intervention

SageMath tutorial at Codima school. 

 * [Some Demos and tutorials on SageMathCloud](https://cloud.sagemath.com/projects/0fb81984-106d-4f23-a423-04478a016e0e/files/Presentations/2016_CODIMA/)
 * [Gap in Sage](http://doc.sagemath.org/html/en/reference/interfaces/sage/interfaces/gap.html)
 * [All the thematic tutorials](http://doc.sagemath.org/html/en/thematic_tutorials/index.html)

