Title: Women in Sage -- Sage Days 98
date: 2019-04-08 00:00
slug: 2019-04-08-conference
lang: en
location: Archanes, Crete
conference_date_start: 2019-04-08
conference_date_end: 2019-04-12
conference_url: https://wiki.sagemath.org/days98
tags: OpenDreamKit, Sage, SageDays, WomenInSage
role: organizer

[See our blog post](https://opendreamkit.org/2019/06/28/WomenInSage/)





