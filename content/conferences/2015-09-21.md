Title: Journées du GT Combinatoire Algébrique du GDR IM 2015
date: 2015-09-21 00:00
slug: 2015-09-21-conference
lang: en
location: LIX
conference_date_start: 2015-09-21
conference_date_end: 2015-09-22
conference_url: http://www.lix.polytechnique.fr/combi/archivesSeminaire/CombAlg15.html
tags: GTCombAlg
role: organizer

The algebraic combinatorics GT is part of the [GDR Im](https://www.gdr-im.fr/).
Every year, an event is organized to gather the French algebraic combinatorics
community. In 2015, the event is held in LIX and jointly by Orsay and LIX teams. 



