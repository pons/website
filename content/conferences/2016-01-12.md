Title: Combinatorics Research School
date: 2016-01-18 00:00
slug: 2016-01-18-conference
lang: en
location: ENS Lyon
conference_date_start: 2016-01-18
conference_date_end: 2015-01-22
course_doc_link: 2016_ENS/
school: True
tags: Sage, Research School
role: invited teacher

The 2015 ENS Lyon Research School on combinatorics and Sage is part of the school
master program along with other research schools opened to the ENS students.



