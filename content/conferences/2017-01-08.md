Title: Women in Sage - Sage Days 82
date: 2017-01-08 00:00
slug: 2017-01-08-conference
lang: en
location: Paris
conference_date_start: 2017-01-08
conference_date_end: 2017-01-13
conference_url: https://wiki.sagemath.org/days82
tags: SageDays, Sage, OpenDreamKit, WomenInSage
role: main organizer

I am the main organizer of the first Women in Sage conference in Europe. This
is a one week workshop on the the software SageMath targeted at women. This
is funded by the [OpenDreamKit](http://opendreamkit.org/) project.

The event was a great success, you can [read this post](http://opendreamkit.org/2017/04/06/WomenInSage/)
which describes our week of work and its immediate impact.




