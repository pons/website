Title: Sage Days 67: PyCon 2015
date: 2015-04-08 00:00
slug: 2015-04-08-talk
lang: en
location: Montreal
conference_date_start: 2015-04-13
conference_date_end: 2015-04-16
conference_url: https://wiki.sagemath.org/days67
tags: SageDays, Sage
role: main organizer

This is a satellite event to [PyCon 2015](https://us.pycon.org/2015) organized
with the help of PyCon and UQAM.



