Title: Algorithmique apprentis Polytech
date: 2018-09-01 00:00
slug: teaching-algopolytech
lang: fr
class_year_begin: 2014
git: https://github.com/VivianePons/courses/tree/master/Algorithmic_Polytech
tags: Algorithmic, L3

L'ensemble des documents de cours se trouvent sur le répertoire git.

La note finale est constituée de 3 notes : 2 partiels et une note de TP. Pour les
modalité de la note de TP, voir avec le chargé de TP.

Année 2020-21 :

**Partiel 1 :** mercredi 17 mars

**Partiel 2 :** jeudi 27 mai

