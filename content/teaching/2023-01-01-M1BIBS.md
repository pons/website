Title: Cours de Programmation Orientée objet M1 BIBS
date: 2023-01-01 00:00
slug: teaching-poobibs
lang: fr
class_year_begin: 2023
git: https://github.com/VivianePons/JavaBIBS
tags: M1, Java, POO

Cours d'introduction à la programmation orientée objet et au Java.

[Voir le site du cours](https://www.lri.fr/~pons/static/JavaBIBS/)


