Title: FindStat
menuTitle: FindStat
lang: fr
slug:findstat
page_order: 2
parentpage: programming
include_tag: findstat
img: findstat.png

Depuis février 2013, je participe en tant que développeur au projet [FindStat](http://www.findstat.org/). 
FindStat est une base de donnée de statistisques combinatoires permettant de rechercher 
et comparer des statistisques sur différents objets mathématiques.
 Le service est accessible à travers un site développé en **Python** grâce à **MoinMoin** et utilise 
[Sage]({filename}sage.md) pour modéliser les relations entre les différents objets. 

## Article

[Findstat - the combinatorial statistics database](https://arxiv.org/abs/1401.3690)

## Exposés



