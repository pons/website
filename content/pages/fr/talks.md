Title: Exposés et posters
menuTitle: Exposés et posters
lang: fr
slug:talks
page_order: 2
parentpage: research
include_category: talks-posters
show_tags: True

Vous trouverez ci-dessous la liste de mes plus récentes présentations, vous pouvez
aussi parcourir la [liste complète](../category/talks-posters.html) ou rechercher
par thématique.

N'hésitez pas à me contacter si vous souhaitez les sources latex d'un exposé ou
d'un poster.
