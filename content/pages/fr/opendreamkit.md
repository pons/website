Title: OpenDreamKit
menuTitle: OpenDreamKit
lang: fr
slug:opendreamkit
page_order: 3
parentpage: programming
include_tag: opendreamkit
img: odk-logo.svg
list_short: true

[OpenDreamKit](http://opendreamkit.org/) (pour Open
Digital Research Environment Toolkit for the Advancement of Mathematics) est un projet européen H2020
qui s'étend de septembre 2015 à août 2019. Le but est le développement et la
promotion des environnements open source en mathématique, en particulier, le logiciel
[SageMath]({filename}sage.md) ainsi que d'autres projets open-source (Gap, Pari, Singular,
...). 

Mon collègue [Nicolas Thiéry](http://nicolas.thiery.name/) est le coordinateur
du projet au niveau européen. Je suis moi-même en charge du site Paris-Sud. Par ailleurs,
je suis responsable du workpackage *Community Building, Training, Dissemination, Exploitation, and Outreach*.

## Évènements récents




