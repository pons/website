Title: Recherche
menuTitle: Ce que je fais
dropdowntitle: Recherche
lang: fr
slug:research
page_order: 3
img: permutree.jpg

**Pour une introduction à mon travail de recherche, vous pouvez lire sur mon blog [Ordonner les ordres](http://openpyviv.com/2017/02/07/ordres/)
qui présente de façon accessible certains de mes résultats. La vidéo ci-dessous est aussi
un bon point de départ.** 

<div style="max-width: 560px; max-height:315px;margin:auto;">
<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/RcXmhKF9ewo" allowfullscreen></iframe>
</div>
</div>



Ma recherche relève de la **combinatoire algébrique**, c'est-à-dire que j'étudie
les structures algébriques, telles que les algèbres de Hopf ou les ordres partiels,
 construites sur des objets combinatoires. Je m'intéresse plus spécifiquement au lien
entre **algèbre de Hopf**, **ordres partiels** et **polytopes**. Mes objets de prédilections
sont les arbres binaires et les permutations et toutes les structures qui leur sont
liées : [treills de Tamari](https://fr.wikipedia.org/wiki/Treillis_de_Tamari), 
ordre faible, [permutoèdre](https://en.wikipedia.org/wiki/Permutohedron), [associaèdre](https://fr.wikipedia.org/wiki/Associa%C3%A8dre)
, etc. 

L'image sur la droite représente un [Permutreehèdre](https://arxiv.org/abs/1606.09643)
imprimé en 3 dimension : ses sommets  sont comptés par des objets qui permettent d'interpoler
entre les arbres binaires et les permutations, 18 dans l'exemple.

Le développement logiciel fait partie intégrante de ma recherche souvent basée
sur **l'exploration par ordinateur**. Le principe est de modéliser des objets 
mathématiques sur machine pour pouvoir expérimenter sur leurs propriétés. 

J'explique ces principes dans cette vidéo : [Talk at Pycon 2015](https://www.youtube.com/watch?v=3LZiZKgVjaU) -- ou  version courte [Lightning talk, SciPy 2014 (30:20)](https://www.youtube.com/watch?v=SMyto7WHiNs&list=PLYx7XA2nY5GfuhCvStxgbynFNrxr3VFog&t=1820) et ici en français [PyConFr 2014](http://t.co/hUDZxDkXrx).



