Title: Programmation
menuTitle: Programmation
lang: fr
slug:programming
page_order: 4


La programmation fait partie intégrante de ma recherche. Beaucoup de mes résultats
s'appuient sur une **exploration** préalable par ordinateur : tests de conjectures,
mises au jour de propriétés, pistes de recherche... Pour mieux comprendre cette approche,
vous pouvez regarder [la présentation]({filename}/talks-posters/2015-04-10.md) 
que j'ai donnéee à PyCon en 2015 sur ce sujet.

Je programme surtout en **python** et plus spécifiquement au sein du logicel **SageMath**.


