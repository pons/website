Title: Enseignement
menuTitle: Enseignement
lang: fr
slug:teaching
page_order: 2
include_category: teaching


J'enseigne l'informatique à l'université Paris-Sud, au sein du [département d'informatique](https://www.dep-informatique.u-psud.fr/)
ainsi qu'à l'école [Polytech](http://www.polytech.u-psud.fr). Ce sont principalement
des cours niveau licence sur des thématiques générales telles que la **programmation** 
et **l'algorithmique**.

Occasionnellement, j'interviens dans des domaines plus spécialisés tels que la
**combinatoire** et le logiciel mathématique [SageMath]({filename}sage.md)
au sein d'écoles de recherche ou de formations en France ou à l'étranger.

Une partie de mon matériel de cours est [disponible sur gitHub](https://github.com/VivianePons/courses)
sous licence Creative Commons.

Vous trouverez ci-dessous la liste de mes enseignements à l'université, vous
pouvez consulter ici mes [interventions en école de recherche]({tag}Research school).

