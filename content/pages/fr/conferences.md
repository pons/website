Title: Conférences
menuTitle: Conférences
lang: fr
slug:conferences
page_order: 3
parentpage: research
include_category: conferences
show_tags: True


Je liste ici les conférences ou écoles de recherche (passées et prévues) où j'ai
joué un rôle au niveau de l'organisation ou d'une intervention importante.
