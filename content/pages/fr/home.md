Title: Présentation
TitleSEO: Viviane Pons - LISN Université Paris-Saclay
menuTitle: Accueil
lang: fr
slug:home
page_order: 1
URL:
save_as: index.html
img: me.jpg
include_category: all
number_articles: 5
list_short: true

Je suis **maîtresse de conférences** à l'[Université Paris-Saclay](http://www.u-psud.fr/)
 depuis le 1er septembre 2014, au sein de [l'équipe GALaC](https://galac.lri.fr/) du [LRI](http://www.lri.fr/).

## Thème de recherche

Ma recherche se situe dans le domaine de la **combinatoire algébrique**. J'étudie 
les structures d'ordres partiels sur les objets combinatoires tels que *l'ordre 
faible sur les permutations* ou le *Treillis de Tamari*. Par ailleurs, je m'intéresse 
au développement logiciel mathématique, en particuier à **l'exploration par ordinateur**
 et contribue au logiciel [Sage](http://sagemath.org/).

[En savoir plus]({filename}research.md) 

## Carrière académique

J'ai soutenu [ma thèse](http://arxiv.org/abs/1310.1805) le 7 octobre 2013 à [l'U-PEM](http://www.u-pem.fr/)
puis j'ai été un an postdoc à [l'université de Vienne](https://mathematik.univie.ac.at/).

## Responsabilités

Je participe au projet [OpenDreamKit]({filename}opendreamkit.md) en tant que responsable
du site Paris-Sud et des aspects liés à la dissémination et au partage des développements
réalisés.


## Récemment...
