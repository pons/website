Title: Publications
menuTitle: Publications
lang: fr
slug:publications
page_order: 1
parentpage: research
template: publications


L'ensemble de mes publications est disponible sur [arXiv](http://arxiv.org/a/pons_v_1)
(avec source latex),
sur [Google Scholar](https://scholar.google.fr/citations?user=tZmJHnAAAAAJ) et sur
[HAL](https://hal.archives-ouvertes.fr/search/index/q/%2A/authIdHal_s/vpons/).

## Habilitation à diriger des recherches

*Combinatorics of the Permutahedra, Associahedra, and Friends* soutenue le 23 octobre 2023 à l'Université Paris-Saclay

[ [arXiv](https://arxiv.org/abs/2310.12687) ][ [HAL](https://hal.science/tel-04390473) ]
