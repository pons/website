Title: Projets
menuTitle: Projets
lang: fr
slug: projects
page_order: 4
parentpage: research

## PAGCAP 2022 -- 2026

### Au delà du Permutoèdre et de l'Associaèdre : géométrie, combinatoire, algèbre et probabilité

Je suis la coordinatrice du project [PAGCAP](https://pagcap.lisn.upsaclay.fr/), c'est un **project de collaboration internationale** entre la France et l'Autriche financé par l'ANR le la FWF. Nous nous situons à l'intersection de l'informatique théorique et des mathématiques fondamentales, avec pour objectifs des questions reliant la combinatoire, la géométrie discrète, l'algèbre, les probabilités libres et l'algorithmique. [Pour en savoir plus, visitez notre site web](https://pagcap.lisn.upsaclay.fr/)


