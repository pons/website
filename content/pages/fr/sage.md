Title: SageMath
menuTitle: SageMath
lang: fr
slug:sage
page_order: 1
parentpage: programming
include_tag: sage
list_short: true
img: sage.png

Sage est logiciel mathématique **libre, open-source et gratuit** écrit en Python. Il est développé
en majeure partie par des chercheurs à l'aide d'un système de contributions et peer review.
Vous trouverez sur le site [sagemath.org](http://www.sagemath.org/) la documentation
et les instructions pour l'installation. La plateforme open-source [SageMathCloud](http://cloud.sagemath.com/)
offre une interface gratuite pour utiliser Sage en ligne ainsi que des comptes payants
pour, par exemple, l'utiliser massivement pour de l'enseignement.

Pour ma recherche, Sage est un outil indispensable et je l'utilise en permanence. 
Il m'arrive aussi de l'utiliser pour enseigner, comme ici [...]. Par ailleurs,
je suis aussi **contributrice** en proposant des développements et ajouts liés
à ma recherche. [L'enseemble de mes contributions](http://trac.sagemath.org/query?author=Viviane%20Pons)
est accessible sur [trac.sagemath.org](http://trac.sagemath.org) qui est le système
utilisé pour développer Sage.

Par ailleurs, je m'investis dans **la promotion et la formation** à Sage en intervenant
dans différentes conférences et en organisant moi-même des évènements en particulier
grâce au projet [OpenDreamKit]({filename}opendreamkit.md). [Voir toutes mes interventions et évènements sur Sage]({tag}Sage).

## Démo Sage / tutoriels

La [documentation de Sage](http://doc.sagemath.org/) offre un grand nombre de
tutoriels de qualité.

Je propose des démos sous forme de notebook Jupyter liés à ma recherche, [disponible sur github](https://github.com/VivianePons/public-notebooks) 
sous licence libre.

## Bases des polynômes multivariés

Ce projet commencé en 2010 représente environ 10000 lignes de code. Il a pour but d'implanter en Sage des bases classiques
de polynômes multivariés basées sur les **différences divisées** : Schubert, Grothedieck,
caractères de Demazure. La définition de ces bases vient en particulier des résultats
d'Alain Lascoux. 

Le projet est distribué en tant que **paquet additionnel dans Sage** et peut
être installé de la façon suivante :

    $ sage -pip install multipolynomial_bases

 * [Le projet sur pypi](https://pypi.python.org/pypi/multipolynomial_bases/)
 * [Le code sur git](https://github.com/VivianePons/multipolynomial-bases)
 * [La documentation](https://www.lri.fr/~pons/sage_docs/multipolynomial_bases/)

Pour en savoir plus, vous pouvez parcourir les [présentations]({tag}Multivariate polynomials)
ou [l'article publié](https://arxiv.org/abs/1207.0398). 


## Évènements récents



