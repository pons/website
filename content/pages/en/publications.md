Title: Publications
menuTitle: Publications
lang: en
slug:publications
page_order: 1
parentpage: research
template: publications


All my papers are available on [arXiv](http://arxiv.org/a/pons_v_1)
(with latex source),
on [Google Scholar](https://scholar.google.fr/citations?user=tZmJHnAAAAAJ), and on
[HAL](https://hal.archives-ouvertes.fr/search/index/q/%2A/authIdHal_s/vpons/).

## Habilitation à diriger des recherches

*Combinatorics of the Permutahedra, Associahedra, and Friends* defended on October, 20 2023 at Paris-Saclay University

[ [arXiv](https://arxiv.org/abs/2310.12687) ][ [HAL](https://hal.science/tel-04390473) ]

