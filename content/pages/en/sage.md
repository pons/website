Title: SageMath
menuTitle: SageMath
lang: en
slug:sage
page_order: 1
parentpage: programming
include_tag: sage
list_short: true
img: sage.png

[SageMath](http://www.sagemath.org/) (or Sage for short) is a **free open-source** mathematic software written in python. It is developed
mostly by researchers with a system based on contributions and peer reviews. On the
[website sagemath.org](http://www.sagemath.org/), you will find the documentation
and installation instructions. The on-line open-source platform [SageMathCloud](http://cloud.sagemath.com/) 
offers a free interface to use Sage on line and also paying accounts for large usage 
(like teaching).

For my research, Sage is my everyday tool. I also, sometimes, use it for teaching [...].
I am also a **contributor** of the combinatorics components for anything that is
related to my research. You can find here [all my contributions](http://trac.sagemath.org/query?author=Viviane%20Pons).

Furthermore, I am very active at **promoting and teaching** Sage in the academic world.
I give presentations in various conferences and also often organize Sage events which
is now supported by my role in the [OpenDreamKit]({filename}opendreamkit.md) project.
[See all my Sage interventions]({tag}Sage).

## Sage Demo / tutorials

The [Sage documentation](http://doc.sagemath.org/) offers many great tutorials.

I offer some demo Jupyter notebooks on [my own github](https://github.com/VivianePons/public-notebooks) under a free licence.


## Bases of multivariate polynomials

This project was started in 2010 represents around 10000 lines of code. Its goal is to implement some classical
bases of multivariate polynomials based on **divided diffences**: Schubert, Grothendieck,
Demazure characters. This mostly comes from the work of Alain Lascoux.

It is distributed as an additional **SageMath package**. If you have a working 
install of Sage, you can install it with this command:

    $ sage -pip install multipolynomial_bases


 * [The project on pypi](https://pypi.python.org/pypi/multipolynomial_bases/)
 * [The code on github](https://github.com/VivianePons/multipolynomial-bases)
 * [The documentation](https://www.lri.fr/~pons/sage_docs/multipolynomial_bases/)

You can know more by looking at the many [presentations]({tag}Multivariate polynomials)
I gave on the subject or read [this paper](https://www.emis.de/journals/SLC/wpapers/s66pons.html).

## Recent Sage events



