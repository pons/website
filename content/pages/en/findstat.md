Title: FindStat
menuTitle: FindStat
lang: en
slug:findstat
page_order: 2
parentpage: programming
include_tag: findstat
img: findstat.png

Since February 2013, I am a developer of the [FindStat](http://www.findstat.org/) project. 
FindStat is a database of combinatorial statistics to allow for search and 
comparisons between different statistics on mathematical objects. It is accessible 
through a website developed in **Python** thanks to MoinMoin. [Sage]({filename}sage.md)
 is used as a backend tool to keep track of relations between the different objects. 

## Article

[Findstat - the combinatorial statistics database](https://arxiv.org/abs/1401.3690)

## Talks



