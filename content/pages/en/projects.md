Title: Projects
menuTitle: Projects
lang: en
slug: projects
page_order: 4
parentpage: research

## PAGCAP 2022 -- 2026

### Beyond Permutahedra and Associahedra: Geometry, Combinatorics, Algebra, and Probability

I am the coordinator of the [PAGCAP project](https://pagcap.lisn.upsaclay.fr/), an ANR -- FWF  **International Collaborative Research Project** between France and Austria running from 2022 to 2026.  It lies at the intersection of theoretical computer science and pure mathematics, aiming to solve problems connecting combinatorics, discrete geometry, algebra, free probability, and algorithmics. [Visit our website to know more](https://pagcap.lisn.upsaclay.fr/)


