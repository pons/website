Title: Presentation
TitleSEO: Viviane Pons - LISN Université Paris-Saclay
menuTitle: Home
lang: en
slug:home
page_order: 1
URL:
save_as: index.html
img: me.jpg
include_category: all
number_articles: 5
list_short: true

I have a position as a *maîtresse de conférences*, *i.e.*, **associate professor**, at
[Paris-Saclay University](http://www.u-psud.fr/) in the [GALaC team](https://galac.lri.fr/) of [LRI](http://www.lri.fr/) since September 2014.

## Research thematics

My research is within the scope of **algebraic combinatorics**. I study partially 
ordered sets of combinatorial objects such as the *weak order on permutations* and 
the *Tamari lattice*. I'm also interested in mathematical software development 
and **computer exploration**. I am an active contibutor of the [SageMath](http://sagemath.org/)
software.

## Academic career

I defended [my thesis](http://arxiv.org/abs/1310.1805) on October 7th, 2013 in [l'U-PEM](http://www.u-pem.fr/).
I was then a postdoc for a year at [Vienna University](https://mathematik.univie.ac.at/).

## Projects

I am a member of the [OpenDreamKit]({filename}opendreamkit.md) project as a lead partner for 
Paris-Sud and in charge of the dissemination work package.

## Recent events



