Title: Programming
menuTitle: Programming
lang: en
slug:programming
page_order: 4

Programming is an important part of my research. Many of my results are obtained
with the help of **computer exploration**: testing conjectures, finding properties,
research clues... If you want a deeper understanding of this approach, you can watch
[the talk]({filename}/talks-posters/2015-04-10.md) I gave on this subject at PyCon2015.

My favorite language for research is **python** and more specifically, **SageMath**.



