Title: Research
menuTitle: Research
lang: en
slug:research
page_order: 3
img: permutree.jpg

**If you don't know anything about combinatorics, you can watch this video where I give an example of a nice result to present my research (French with Engligh subtitles).**

<div style="max-width: 560px; max-height:315px;margin:auto;">
<div class="embed-responsive embed-responsive-16by9">
<iframe width="560" height="315" class="embed-responsive-item" src="https://www.youtube.com/embed/RcXmhKF9ewo" allowfullscreen></iframe>
</div>
</div>

My research field is **algebraic combinatorics**: I study algebraic structures such
as Hopf algebras and partial orders applied on combinatorial objects. I am especially
interested in the links between **Hopf algebras**, **partial orders**, and **polytopes**.
My favorite objects are permutations and binary trees along with all their associated
structures: [the Tamari lattice](https://en.wikipedia.org/wiki/Tamari_lattice), 
the weak order, [the Permutohedron]((https://en.wikipedia.org/wiki/Permutohedron),
[the Associahedron](https://en.wikipedia.org/wiki/Associahedron), etc.


The image on the right is a 3D-printed [Permutreehedron](https://arxiv.org/abs/1606.09643):
its vertices are counted by some objects, interpolating in-between permutations and 
binary trees (18 in the example).

Software development is an important part of my research often based on
**computer exploration**. The idea is to model mathematical objects through code
to allow for experimentation on their properties. 

I'm explaining this over here: [Talk at Pycon 2015](https://www.youtube.com/watch?v=3LZiZKgVjaU) -- short version [Lightning talk, SciPy 2014 (30:20)](https://www.youtube.com/watch?v=SMyto7WHiNs&list=PLYx7XA2nY5GfuhCvStxgbynFNrxr3VFog&t=1820).



