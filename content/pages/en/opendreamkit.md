Title: OpenDreamKit
menuTitle: OpenDreamKit
lang: en
slug:opendreamkit
page_order: 3
parentpage: programming
include_tag: opendreamkit
img: odk-logo.svg
list_short: true

[OpenDreamKit](http://opendreamkit.org/) (as for Open
Digital Research Environment Toolkit for the Advancement of Mathematics) is a European
H2020 project which goes from September 2015 up to August 2019. The goal is to support
open-source development in mathematics, [SageMath]({filename}sage.md) in particular,
but also other softwares like Gap, Pari, Singular...

The project coordinator is my colleague  [Nicolas Thiéry](http://nicolas.thiery.name/).
I am my self in charge of the Paris-Sud site. I am also the workpackage leader 
for *Community Building, Training, Dissemination, Exploitation, and Outreach*.


## Recent events where I was involved




