Title: Teaching
menuTitle: Teaching
lang: en
slug:teaching
page_order: 2
include_category: teaching

I teach computer science at Paris-Sud University at the [CS department](https://www.dep-informatique.u-psud.fr/)
and [Polytech school](http://www.polytech.u-psud.fr). These are mostly undergrad 
classes on general topics such as **algorithmic** and **programming**.

I sometimes teach more advanced subjects in conferences and research schools
 such as **combinatorics** and the mathematical software [SageMath]({filename}sage.md).

Some of my [teaching material](https://github.com/VivianePons/courses) is available
on gitHub under a creative commons license. 

I am listing here my teaching at university, you can also find all my 
[interventions in Research School]({tag}Research school).

