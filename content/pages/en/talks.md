Title: Talks and Posters
menuTitle: Talks and Posters
lang: en
slug:talks
page_order: 2
parentpage: research
include_category: talks-posters
show_tags: True

You will find below my most recent presentations, you can also go through the 
[full list](../category/talks-posters.html) or look by thematic.

Please contact me if you need the latex source for any of the presentations.

