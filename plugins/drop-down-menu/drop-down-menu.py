"""
Drop Down Menu
==============

Adds a subpages parameters to pages to allow for dropdown menu
"""

from pelican import signals

def get_all_pages(generator):
    d = dict()
    for page in generator.pages:
        d[page.slug] = page
    for page in generator.hidden_pages:
        d[page.slug] = page
    return d

def set_subpages(generator):
    d = get_all_pages(generator)
    for page in d.values():
        if hasattr(page,'parent'):
            parent = d[page.parent]
            if not hasattr(parent,"subpages"):
                parent.subpages = []
            parent.subpages.append(page)
    for page in d.values():
        if hasattr(page, "subpages"):
            page.subpagesmenu = True
            page.subpagespage = True
            if hasattr(page, "showsubpages"):
                if page.showsubpages == "menu":
                    page.subpagesmenu = True
                    page.subpagespage = False
                elif page.showsubpages == "page":
                    page.subpagesmenu = False
                    page.subpagespage = True
            if not hasattr(page,"dropdowntitle") and hasattr(page,"menutitle"):
                page.dropdowntitle = page.menutitle



def register():
    signals.page_generator_finalized.connect(set_subpages)
