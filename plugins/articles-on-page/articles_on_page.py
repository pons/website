"""
Articles on page
=================

Allows a list of articles to be attached to a page

Author: Viviane Pons

"""

from pelican import signals

def set_for_page(page, article_generator, number_articles):
    if hasattr(page, "include_category"):
        page_category = page.include_category
        if not hasattr(page,"number_articles"):
            page.number_articles = number_articles
        c = int(page.number_articles)
        page_articles = None
        if page_category != "all":
            for category, articles in article_generator.categories:
                if category == page_category:
                    page_articles = articles
                    page.category_url = category.url
                    page.category_number = len(page_articles)
                    break
        else:
            page_articles = article_generator.articles
            page.category_url = article_generator.settings["INDEX_SAVE_AS"]
        page.articles = []

        if page_articles is not None:
            for article in page_articles:
                page.articles.append(article)
                c-=1
                if c == 0:
                    break
    if hasattr(page, "include_tag"):
        page_tag = page.include_tag
        if not hasattr(page,"number_articles"):
            page.number_articles = number_articles
        c = int(page.number_articles)
        page_articles = None

        for tag in article_generator.tags:
            if tag == page_tag:
                page_articles = article_generator.tags[tag]
                page.category_url = tag.url
                page.category_number = len(page_articles)
                break

        page.articles = []
        if page_articles is not None:
            for article in page_articles:
                page.articles.append(article)
                c-=1
                if c == 0:
                    break


def set_articles_on_pages(generators):
    article_generator = generators[0]
    page_generator = generators[1]
    if "ARTICLES_ON_PAGE" in article_generator.settings:
        number_articles = article_generator.settings["ARTICLES_ON_PAGE"]
    else:
        number_articles = 10
    for page in page_generator.pages:
        set_for_page(page, article_generator, number_articles)

    for page in page_generator.hidden_pages:
        set_for_page(page, article_generator, number_articles)

def register():
    signals.all_generators_finalized.connect(set_articles_on_pages)
