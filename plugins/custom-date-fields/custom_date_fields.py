"""
Custom Date fieds
=================

Transform a list of specied custom fields into datetime object

Author : Viviane Pons

"""

from pelican import signals
import datetime

def set_for_article(article, custom_keys):
    for key in custom_keys:
        if hasattr(article, key):
            datestring = getattr(article, key)
            setattr(article, key, datetime.datetime.strptime(datestring,"%Y-%m-%d"))


def set_custom_date_fields_articles(generator):
    if "CUSTOM_DATE_FIELDS" not in generator.settings:
        return
    custom_keys = generator.settings["CUSTOM_DATE_FIELDS"]
    for article in generator.articles:
        set_for_article(article, custom_keys)

def register():
    signals.article_generator_finalized.connect(set_custom_date_fields_articles)
