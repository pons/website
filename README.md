This is my academic website sourcefiles. The website is generated using [Pelican](http://getpelican.com/), 
the theme is homemade and based on [Bootstrap](http://getbootstrap.com/).

Unless otherwise stated, the homemade code is under a GNU General Public License (GPL), the content (content folder)
is under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).